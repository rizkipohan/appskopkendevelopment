<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';

    protected $fillable = [
        'name', 'qty', 'price','department_id','show'
    ];

    public function itemout()
    {
        return $this->hasMany('App\ItemOut');
    }

    public function itemin()
    {
        return $this->hasMany('App\ItemIn');
    }
}
