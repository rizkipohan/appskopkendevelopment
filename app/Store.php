<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'stores';

    // protected $fillable = [
    //     'store_name','store_code','store_status','store_address','store_phone','store_email','tax_number','open_date',
    //     'connection_status','provider_id','sid','mikrotik_status','ip_local','ip_wan',
    //     'ip_gateway_wan','ip_dns','ip_vpn'
    // ];

    protected $guarded = [];


    // public function provider()
    // {
    //     return $this->belongsTo('App\Provider');
    // }

    public function itemout()
    {
        return $this->hasMany('App\ItemOut');
    }

    public function bast()
    {
        return $this->hasOne('App\Bast');
    }

    public function nso()
    {
        return $this->hasOne('App\Nso');
    }
}
