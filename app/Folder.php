<?php

namespace App;

use BadChoice\Thrust\Fields\HasMany;
use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $fillable  = [
        'name',
    ];

    public function file()
    {
        return $this->hasMany('App\File');
    }
}
