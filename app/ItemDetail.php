<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemDetail extends Model
{
    use SoftDeletes;

    protected $table = 'mst_item_detail';

    protected $guarded = [];

    public function itemname()
    {
        return $this->belongsTo('App\ItemName','item_name_id');
    }

    public function itemtype()
    {
        return $this->belongsTo('App\ItemType','item_type_id');
    }

    public function itemin()
    {
        return $this->hasMany('App\Trxitemin');
    }

    public function itemout()
    {
        return $this->hasMany('App\Trxitemout');
    }

    // public function last_location()
    // {
    //     return $this->hasOne('App\Trxitemout','item_detail_id')->latest->location->store_name;
    // }

    public function vendor()
    {
        return $this->belongsTo('App\VendorModel','vendor_id');
    }

    public function class_model()
    {
        return $this->belongsTo('App\ClassModel','class_id');
    }

    public function dept()
    {
        return $this->belongsTo('App\DeptModel','dept_id');
    }

    public function designation()
    {
        return $this->belongsTo('App\VendorModel','designation_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Store','location_id');
    }
}
