<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassModel extends Model
{
    use SoftDeletes;

    protected $table = 'mst_class';

    protected $guarded = [];

    // public function itemname()
    // {
    //     return $this->hasMany('App\ItemName');
    // }
}
