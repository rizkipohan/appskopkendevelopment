<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nso extends Model
{
    protected $table = 'nso';

    protected $fillable = [
        'store_id',
        'json'
    ];

    // public function provider()
    // {
    //     return $this->belongsTo('App\Provider');
    // }

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
