<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Store;
use App\ItemOut;
use App\RequestItem;
use Auth;
use Mail;
use App\Mail\SendRequest;
use App\Mail\SendRequestNotif;
use App\Mail\SendApproval;
use App\Mail\AdminNotification;
use App\Mail\YourRequestItem;
use App\User;

use function GuzzleHttp\json_decode;

class RequestItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getStore()
    {
        $get = Store::all();
        return $get;
    }

    public function getItem()
    {
        $get = Item::where('department_id',Auth::user()->department_id)->get();
        return $get;
    }

    public function getAdmin()
    {
        $get = User::where('it_admin',1)->first();
        return $get;
    }

    public function index()
    {
        $data = ItemOut::all();
        return view('requestitem.index',['reqitem'=>$data]);
    }

    public function create()
    {
        $items = $this->getItem();
        $opt="";
        foreach($items as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        // dd($opt);
        return view('requestitem.create',['stores'=>$this->getStore(), 'items'=>$this->getItem(),'opt'=>$opt]);
    }

    public function store(Request $request)
    {

        $count = count($request->item_id);

        for($i=0;$i<$count;$i++){
            $itemname = Item::where('id',$request->item_id[$i])->get('name');
            for($j=0;$j<5;$j++){
                $item_arr[$i]['item_id'] = $request->item_id[$i];
                $item_arr[$i]['item_name'] = $itemname[0]->name;
                $item_arr[$i]['serial_number'] = $request->serial_number[$i];
                $item_arr[$i]['qty'] = $request->qty[$i];
                $item_arr[$i]['status'] = $request->status[$i];
            }
        }

        $data = (object) $request->all();
        $user =  Auth::user();
        $items = $item_arr;
        // dd($data,$items[0]['item_name']);
        RequestItem::firstOrCreate([
            'store_id' => $request->store_id,
            'items'=>json_encode($item_arr),
            'created_by'=> $user->id,
            'approve_status'=> '0'
        ]);

        $get_id = RequestItem::where('store_id',$request->store_id)->where('approve_status','0')->first();
        $id = $get_id->id;

        Mail::to($this->getAdmin()->email)->send(new SendRequest($data,$items,$user,$id));
        Mail::to($user->email)->send(new YourRequestItem($data,$items,$user,$id));

        // Mail::to($user->email);
        return redirect('requestitem/create')->with('success','berhasil submit request!');
    }

    public function edit($id)
    {
        $get = RequestItem::where('id',$id)->where('approve_status','0')->first();
        // dd($get);
        $items = json_decode($get->items,true);
        // dd($get[0]->items);
        return view('requestitem.edit',['reqs'=>$get,'items'=>$items,'id'=>$id]);
    }

    public function insertItemOut(Request $request,$id)
    {
        for($i=0;$i<count($request->item_id);$i++)
        {
            ItemOut::create([
                'item_id' => $request->item_id[$i],
                'store_id' => $request->store_id,
                'qty' => $request->qty[$i],
                'status' => $request->status[$i],
                'serial_number' => $request->serial_number[$i],
                'date' => $request->date,
                'request_item_id' => $id
            ]);
        }
    }

    public function update($id,Request $request)
    {

        $reqs = RequestItem::findOrFail($id);
        $items = json_decode($reqs->items,true);
        $user = $reqs->user;
        $admin = $this->getAdmin();
        // dd($user->email,$admin->email);
        if($request->approval == "1" && $reqs->approve_status == '0')
        {
            $reqs->update([
                'approve_status' => $request->approval
            ]);

            $this->insertItemOut($request,$reqs->id);

            Mail::to($user->email)->send(new SendApproval($user,$reqs,$items));
            Mail::to($admin->email)->send(new AdminNotification($admin,$reqs,$items));

            return redirect('requestitem/create')->with('success','berhasil approve!');
        }


    }

}
