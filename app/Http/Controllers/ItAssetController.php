<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Store;
use App\ItemType;
use App\ItemName;
use App\VendorModel;
use App\ItemDetail;
use App\Trxitemin;
use App\ClassModel;
use App\DeptModel;
use App\Designation;
use App\Trxitemout;
use App\DataTables\ItemDetailDataTable;
// use Codedge\Fpdf\Fpdf\Fpdf;
use PDF;
// use PDF2;
use DataTables;


class ItAssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('itasset.app');
    }

    /*
    =======================================================================================
    ITEM TYPE
    =======================================================================================
    */
    public function itemTypeIndex(){
        $gets = ItemType::all();
        return view('itasset.master.itemtype.index',['gets'=>$gets]);
    }

    public function itemTypeDelete($id)
    {
        $get = ItemType::find($id);
        $get->delete();

        return redirect()->back()->with('success','berhasil hapus item type');
    }

    public function getItemTypejq(){
        $gets = ItemType::all();
        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'data-code='".$item->code_for_asset."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function addItemTypejq(Request $request){
        ItemType::create([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset,
            'created_by' => Auth::user()->id
        ]);
        return response()->json(['status' => 200]);
    }

    public function itemTypeEdit($id){
        $get = ItemType::find($id);

        return view('itasset.master.itemtype.edit',['items'=>$get]);
    }

    public function itemTypeUpdate($id, Request $request){
        $get = ItemType::find($id)->update([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset
        ]);

        return redirect('itasset/master/itemtype/index')->with('success','berhasil edit item type');
    }

    /*
    =======================================================================================
    ITEM NAME
    =======================================================================================
    */
    public function itemNameIndex(){
        $gets = ItemName::all();
        // dd($gets[0]->itemtype);
        return view('itasset.master.itemname.index',['gets'=>$gets]);
    }

    public function itemNameDelete($id)
    {
        $get = ItemName::find($id);
        $get->delete();

        return redirect()->back()->with('success','berhasil hapus item name');
    }

    public function getItemNameByTypejq(Request $request){

        $gets = ItemName::where('item_type_id',$request->id)->get();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'data-code='".$item->code_for_asset."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function getItemNamejq(){
        $gets = ItemName::all();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'data-code='".$item->code_for_asset."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function addItemNamejq(Request $request){
        ItemName::create([
            'name' => $request->name,
            'item_type_id' => $request->itemtype,
            'code_for_asset' => $request->code_for_asset,
            'created_by' => Auth::user()->id
        ]);
        return response()->json(['status' => 200]);
    }

    public function itemNameEdit($id){
        $get = ItemName::find($id);
        $type = ItemType::all();
        return view('itasset.master.itemname.edit',['items'=>$get,'type'=>$type]);
    }

    public function itemNameUpdate($id, Request $request){
        $get = ItemName::find($id)->update([
            'name' => $request->name,
            'item_type_id' => $request->itemtype,
            'code_for_asset' => $request->code_for_asset
        ]);

        return redirect('itasset/master/itemname/index')->with('success','berhasil edit item name');
    }


    /*
    =======================================================================================
    MASTER VENDOR
    =======================================================================================
    */
    public function vendorIndex(){
        $gets = VendorModel::all();
        // dd($gets[0]->itemtype);
        return view('itasset.master.vendor.index',['gets'=>$gets]);
    }

    public function vendorDelete($id)
    {
        $get = VendorModel::find($id);
        $get->delete();

        return redirect()->back()->with('success','berhasil hapus vendor');
    }

    public function getVendorjq(){
        $gets = VendorModel::all();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function addVendorjq(Request $request){
        VendorModel::create([
            'name' => $request->name,
            'created_by' => Auth::user()->id
        ]);
        return response()->json(['status' => 200]);
    }

    public function vendorEdit($id){
        $get = VendorModel::find($id);
        // $type = ItemType::all();
        return view('itasset.master.vendor.edit',['items'=>$get]);
    }

    public function vendorUpdate($id, Request $request){
        $get = VendorModel::find($id)->update([
            'name' => $request->name,
        ]);

        return redirect('itasset/master/vendor/index')->with('success','berhasil edit vendor');
    }

    /*
    =======================================================================================
    MASTER CLASS
    =======================================================================================
    */
    public function classIndex(){
        $gets = ClassModel::all();
        return view('itasset.master.class.index',['gets'=>$gets]);
    }

    public function getClassjq(){
        $gets = ClassModel::all();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'data-code='".$item->code_for_asset."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function classDelete($id)
    {
        $get = ClassModel::find($id);
        $get->delete();

        return redirect()->back()->with('success','berhasil hapus class');
    }

    public function addClassjq(Request $request){
        ClassModel::create([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset,
            'created_by' => Auth::user()->id
        ]);
        return response()->json(['status' => 200]);
    }

    public function classEdit($id){
        $get = ClassModel::find($id);

        return view('itasset.master.class.edit',['items'=>$get]);
    }

    public function classUpdate($id, Request $request){
        $get = ClassModel::find($id)->update([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset
        ]);

        return redirect('itasset/master/class/index')->with('success','berhasil edit class');
    }

    /*
    =======================================================================================
    MASTER DEPT
    =======================================================================================
    */
    public function deptIndex(){
        $gets = DeptModel::all();
        return view('itasset.master.dept.index',['gets'=>$gets]);
    }

    public function getDeptjq(){
        $gets = DeptModel::all();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'data-code='".$item->code_for_asset."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function deptDelete($id)
    {
        $get = DeptModel::find($id);
        $get->delete();

        return redirect()->back()->with('success','berhasil hapus dept');
    }

    public function addDeptjq(Request $request){
        DeptModel::create([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset,
            'created_by' => Auth::user()->id
        ]);
        return response()->json(['status' => 200]);
    }

    public function deptEdit($id){
        $get = DeptModel::find($id);

        return view('itasset.master.dept.edit',['items'=>$get]);
    }

    public function deptUpdate($id, Request $request){
        $get = DeptModel::find($id)->update([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset
        ]);

        return redirect('itasset/master/dept/index')->with('success','berhasil edit dept');
    }

    /*
    =======================================================================================
    MASTER DESIGNATION
    =======================================================================================
    */
    public function designationIndex(){
        $gets = Designation::all();
        return view('itasset.master.designation.index',['gets'=>$gets]);
    }

    public function getDesigntaionjq(){
        $gets = Designation::all();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'data-code='".$item->code_for_asset."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function designationDelete($id)
    {
        $get = Designation::find($id);
        $get->delete();

        return redirect()->back()->with('success','berhasil hapus designation');
    }

    public function addDesignationjq(Request $request){
        Designation::create([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset,
            'created_by' => Auth::user()->id
        ]);
        return response()->json(['status' => 200]);
    }

    public function designationEdit($id){
        $get = Designation::find($id);

        return view('itasset.master.designation.edit',['items'=>$get]);
    }

    public function designationUpdate($id, Request $request){
        $get = Designation::find($id)->update([
            'name' => $request->name,
            'code_for_asset' => $request->code_for_asset
        ]);

        return redirect('itasset/master/designation/index')->with('success','berhasil edit designation');
    }

    /*
    =======================================================================================
    MASTER LOCATION
    =======================================================================================
    */
    public function locationIndex(){
        return view('itasset.master.location.index');
    }

    public function dataLocationIndex()
    {
        $all = Store::all();

        return Datatables::of($all)
        ->addIndexColumn()
        ->editColumn('button', function(Store $store) {
            return '
                <a class="btn btn-warning" href=edit/'.$store->id.'>Edit</a>
                <a class="btn btn-danger" href=delete/'.$store->id.'>Delete</a>
            ';
        })
        ->rawColumns(['button'])
        ->make(true);
    }

    public function getLocationjq(){
        $gets = Store::all();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->store_name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function locationDelete($id)
    {
        $get = Store::find($id);
        $get->delete();

        return redirect()->back()->with('success','berhasil hapus location');
    }

    public function addLocationjq(Request $request){
        Store::create([
            'store_name' => $request->name,
            'code_for_asset' => $request->code_for_asset,
            'created_by' => Auth::user()->id
        ]);
        return response()->json(['status' => 200]);
    }

    public function locationEdit($id){
        $get = Store::find($id);

        return view('itasset.master.location.edit',['items'=>$get]);
    }

    public function locationUpdate($id, Request $request){
        $get = Store::find($id)->update([
            'store_name' => $request->name,
            'code_for_asset' => $request->code_for_asset
        ]);

        return redirect('itasset/master/location/index')->with('success','berhasil edit location');
    }

    //========================= Generate Code =================================
    public function selectGenerateType(){
        return view('itasset.generate.create');
    }

    public function chooseGeneratePage(Request $request){
        if($request->type=="1"){
            dd("halaman generate it asset code");
        }else{
            dd("halaman input fam code");
        }
    }


    /*
    =======================================================================================
    ITEM DETAIL
    =======================================================================================
    */
    public function itemDetailCreate(){
        return view('itasset.itemdetail.create');
    }

    public function itemDetailStore(Request $request){ 
        for($i=0;$i<$request->qty;$i++){
            //======= Input ke tabel item detail =======
            $itemdetail = ItemDetail::create([
                'fam_code' => $request->fam_code[$i],
                'item_type_id' => $request->itemtype,
                'item_name_id' => $request->itemname,
                'vendor_id' => $request->vendor,
                'is_stock' => 1,
                'created_by' => Auth::user()->id
            ]);
            //====== Input ke table transaksi item in =======
            Trxitemin::create([
                'pr_number' => $request->pr_number,
                // 'item_type_id' => $request->itemtype,
                // 'item_name_id' => $request->itemname,
                'item_detail_id' => $itemdetail->id,
                'vendor_id' => $request->vendor,
                'created_by' => Auth::user()->id
            ]);
        }

        return redirect('itasset/itemdetail/index')->with('success','berhasil tambah item');
    }

    public function itemDetailIndex(){
        // $dd = ItemDetail::find(7);
        // dd($dd->last_location);
        return view('itasset.itemdetail.index');
    }

    public function itemDetailIndex2(ItemDetailDataTable $datatable)
    {
        return $dataTable->render('itasset.itemdetail.index');
    }

    public function dataItemDetail()
    {
        $model = ItemDetail::with(['itemname','itemtype','vendor','location','itemin','itemout']);

        return DataTables::of($model)
        ->addIndexColumn()
        ->addColumn('vendor', function (ItemDetail $trx) {
            return $trx->vendor->name;
        })
        ->addColumn('asset_code', function (ItemDetail $trx) {
            return $trx->it_asset_code.$trx->increment_id;
        })
        ->editColumn('btn-fam', function(ItemDetail $trx) {
            return '
                <a class="btn btn-warning btn-sm" href=edit/famcode/'.$trx->id.'>Edit FAM or SN</a>
            ';
        })
        ->editColumn('btn-cancel', function(ItemDetail $trx) {
            return '
                <a class="btn btn-info btn-sm" href=cancel/booking/'.$trx->id.'>Cancel Booking</a>
            ';
        })
        ->editColumn('btn-delete', function(ItemDetail $trx) {
            return '
                <a class="btn btn-danger btn-sm" href=delete/item/'.$trx->id.'>Delete</a>
            ';
        })
        // ->addColumn('itemname', function (ItemDetail $trx) {
        //     return $trx->itemname->name;
        // })
        // ->addColumn('itemtype', function (ItemDetail $trx) {
        //     return $trx->itemtype->name;
        // })
       
        ->rawColumns(['btn-fam','btn-cancel','btn-delete'])
        ->make(true);
    }
    
    public function editfamcode($id){
        $item = ItemDetail::find($id);
        return view('itasset.itemdetail.editfamcode',['item'=>$item]);
    }

    public function updatefamcode($id,Request $request){
        $item = ItemDetail::find($id);
        $item->update([
            'fam_code' => $request->fam_code,
            'serial_number' => $request->serial_number
        ]);
        return redirect('itasset/itemdetail/index')->with('success','berhasil edit fam code');
    }

    public function cancelBooking($id){
        $item = ItemDetail::find($id);
        $item->update([
            'is_booked' => 0,
            'is_stock' => 1,
            'class_id' => null,
            'dept_id' => null,
            'designation_id' => null,
            'location_id' => null,
            'it_asset_code' => null,
            'increment_id' => null
        ]);

        return redirect()->back()->with(['success'=>'berhasil cancel booking']);
    }

    public function deleteItem($id){
        $item = ItemDetail::find($id);
        $item->delete();

        return redirect()->back()->with(['success'=>'berhasil hapus item']);
    }


   

    /*
    =======================================================================================
    BOOKING ITEM
    =======================================================================================
    */

    public function bookingCreate(){
        $opt = $this->getItemBookjq();
        return view('itasset.booking.create',['opt'=>$opt]);
    }

    public function getItemBookjq(){
        $gets = ItemDetail::where('is_booked',0)->get();

        $opt="";
        foreach($gets as $item)
        {
            // $fill = "<option
            //     value='".$item->id."'
            //     data-type='".$item->item_type_id."'
            //     data-name='".$item->item_name_id."'
            //     >"
            //     .$item->itemtype->name." ".$item->itemname->name." ( FAM: ".$item->fam_code.")
            //     </option>";
            $fill = "<option value='".$item->id."'>".$item->id.". ".$item->itemtype->name." ".$item->itemname->name."( FAM: ".$item->fam_code.")</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function getClassBookjq(){
        $gets = ClassModel::whereNotNull('code_for_asset')->get();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function getDeptBookjq(){
        $gets = DeptModel::whereNotNull('code_for_asset')->get();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function getDesignationBookjq(){
        $gets = Designation::whereNotNull('code_for_asset')->orderby('id','desc')->get();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function getLocationBookjq(){
        $gets = Store::whereNotNull('code_for_asset')->get();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->store_name."</option>";
            $opt = $opt.$fill;
        }
        return $opt;
    }

    public function bookingItem(Request $request){

        for($i=0;$i<count($request->item_id);$i++){
            $cekitem = ItemDetail::where('id',$request->item_id[$i])->first();

            if($cekitem->it_asset_code == null){
    
                $class = ClassModel::find($request->class,['code_for_asset'])->code_for_asset;
                $dept = DeptModel::find($request->dept,['code_for_asset'])->code_for_asset;
                $designation = Designation::find($request->designation,['code_for_asset'])->code_for_asset;
                $location = Store::find($request->location,['code_for_asset'])->code_for_asset;
                $type = $cekitem->itemtype->code_for_asset;
    
                $generate = $class.$dept.$designation.$location.$type;
                $cek = ItemDetail::where('it_asset_code',$generate)
                ->orderBy('increment_id','DESC')
                ->first();
                $asset_code = $generate;
    
                if($cek==null){
                    $increment = "01";
                }else{
                    $str_inc = substr($cek->increment_id,-2);
                    $new = (int) $str_inc + 1;
                    $new = str_pad($new, 2, "0", STR_PAD_LEFT);
                    $increment = $new;
                }
    
                ItemDetail::find($request->item_id[$i])->update([
                    'it_asset_code' => $asset_code,
                    'increment_id' => $increment,
                    'is_booked' => 1,
                    'is_stock' => 0,
                    'class_id'=> $request->class,
                    'dept_id' => $request->dept,
                    'designation_id' => $request->designation,
                    'location_id' => $request->location
                ]);
    
            }
        }

        return redirect('itasset/itemdetail/index')->with('success','berhasil booking item');
    }

    /*
    =======================================================================================
    Check Out Item
    =======================================================================================
    */

    public function selectLocation(){
        return view('itasset.checkout.selectLocation');
    }

    public function showBookedItem(Request $request){
        $items = ItemDetail::where('location_id',$request->location)
        ->where('is_booked',1)
        ->where('is_checkout',0)
        ->get();

        return view('itasset.checkout.createCheckout',['items'=>$items]);
    }

    public function checkOutItem(Request $request)
    {
        for($i=0;$i<count($request->item_detail_id);$i++)
        {
            $item = ItemDetail::find($request->item_detail_id[$i]);

            $item->update([
                'serial_number' => $request->serial_number[$i],
                'is_checkout' => 1,
            ]);

            Trxitemout::create([
                'item_detail_id' => $item->id,
                'location_id' => $item->location_id,
                'is_checkout'=>1,
                'created_by' => Auth::user()->id
            ]);
        }

        return redirect()->route('item_index')->with('success','berhasil checkout item');
    }

    /*
    =======================================================================================
    Item Transfer
    =======================================================================================
    */

    public function trfInSelectLocation(){
        $gets = ItemDetail::where('is_checkout',1)->get();

        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->id.". ".$item->itemtype->name." ".$item->itemname->name."( FAM: ".$item->fam_code.")</option>";
            $opt = $opt.$fill;
        }

        return view('itasset.transfer.inSelectLocation',['opt'=>$opt]);
    }

    public function trfInCreate(Request $request){
        $items = Trxitemout::where('location_id',$request->location)
        ->where('is_checkout',1)->where('is_trfin',null)
        ->get();
        return view('itasset.transfer.inCreate',['items'=>$items]);
    }

    public function trfInStore(Request $request)
    {
        // dd($request->all());
        for($i=0;$i<count($request->item);$i++)
        {
            Trxitemin::create([
                'item_detail_id' => $request->item[$i],
                'from_location' => $request->location[$i],
                'created_by' => Auth::user()->id
            ]);

            Trxitemout::where('location_id',$request->location[$i])
            ->where('item_detail_id',$request->item[$i])->latest('created_at')->first()
            ->update(['is_trfin'=>1]);

            ItemDetail::find($request->item[$i])->update([
                'is_stock' => 1
            ]);
            
        }

        return redirect()->route('item_index')->with('success','berhasil transfer in');
        
    }

    public function trfOutCreate(){
        $gets = ItemDetail::where('is_stock',1)->get();
        $opt="";
        foreach($gets as $item)
        {
            $fill = "<option value='".$item->id."'>".$item->id.". ".$item->itemtype->name." ".$item->itemname->name."( FAM: ".$item->fam_code.")</option>";
            $opt = $opt.$fill;
        }
        return view('itasset.transfer.outCreate',['opt'=>$opt]);
    }

    public function trfOutStore(Request $request)
    {
        // dd($request->all());
        for($i=0;$i<count($request->item);$i++)
        {

            Trxitemout::create([
                'item_detail_id' => $request->item[$i],
                'location_id' => $request->location,
                'created_by' => Auth::user()->id
            ]);

            ItemDetail::find($request->item[$i])->update([
                'is_stock' => 0
            ]);
            
        }

        return redirect()->route('item_index')->with('success','berhasil transfer in');
        
    }
    
    private $fpdf;

    public function printLable($id)
    {
        $data = ItemDetail::find($id);
        // if ($data->fam_code != null || $data->it_asset_code != null){
            $customPaper = array(0,0,56.69,243.78);
            $pdf = PDF::loadView('itasset.printlable',['data' => $data]);
            return $pdf->setPaper($customPaper)->stream();
        // }else{
        //     return redirect()->route('item_index')->with('error','fam code and ');
        // }

        // $TEST_LABEL_DATA = array(

        //     array(
        //         'title' => 'Mr',
        //         'fullName' => 'Bean',
        //         'address' => '10 Bean st.',
        //         'zipCode' => '10000',
        //         'city' => 'London'),

        //     array(
        //         'title' => 'Ms',
        //         'fullName' => 'Piggy',
        //         'address' => '20 Muppet st.',
        //         'zipCode' => '10001',
        //         'city' => 'New York'),

        //     array(
        //         'title' => 'Herr',
        //         'fullName' => 'Vladimir Putin',
        //         'address' => '1 Weapon st.',
        //         'zipCode' => '1000',
        //         'city' => 'Moscow'),

        //     array(
        //         'title' => 'Mr',
        //         'fullName' => 'Bean',
        //         'address' => '10 Bean st.',
        //         'zipCode' => '10000',
        //         'city' => 'London'),

        //     array(
        //         'title' => 'Ms',
        //         'fullName' => 'Piggy',
        //         'address' => '20 Muppet st.',
        //         'zipCode' => '10001',
        //         'city' => 'New York'),

        //     array(
        //         'title' => 'Herr',
        //         'fullName' => 'Vladimir Putin',
        //         'address' => '1 Weapon st.',
        //         'zipCode' => '1000',
        //         'city' => 'Moscow'),
        // );

        // // set page
        // $page_orientation = 'P';
        // $page_units = 'mm';
        // $page_format = 'A4';

        // // set top and left margin
        // $top_margin = 8;
        // $left_margin = 0;

        // // initialize object
        // // $pdf = new PDF2($page_orientation, $page_units, $page_format, true, 'UTF-8');


        // // set margins
        // PDF2::SetMargins(0, 0, -1, true);

        // //set auto page breaks
        // PDF2::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // // set font
        // PDF2::SetFont('times', '', 10);

        // // add a page
        // PDF2::AddPage();

        // // set cell padding
        // PDF2::setCellPadding(1);

        // // set cell margins
        // PDF2::setCellMargins(0, 0, 0, 0);

        // // set label dimensions (see the Avery label specification)
        // $lbl_width = 70;
        // $lbl_min_height = 35;

        // // set the number of  rows and columns on the sheet (see the Avery label specification)
        // $max_rows = 8;
        // $max_cols = 3;

        // // set the position of he first label
        // $x = $top_margin;
        // $y = $left_margin;

        // // set the label column and row count (for line breaks and page breaks)
        // $current_row = 1;
        // $current_col = 1;
        // $total_labels_count = 0;

        // // Read label data from the database and use while loop;
        // // I will use the $TEST_LABEL_DATA array and foreach loop
        // foreach($TEST_LABEL_DATA as $label) {

        //     // calculate label position (you can improve this)
        //     $x = (($current_col - 1) * $lbl_width);
        //     $y = (($current_row - 1) * $lbl_min_height);

        //     // create a label text
        //     $lbl_txt  = $label['title'] . ' ' . $label['fullName'] . "\n";
        //     $lbl_txt .= $label['address'] . "\n";
        //     $lbl_txt .= $label['zipCode'] . ' ' . $label['city'];

        //     // add cell (a label)
        //     PDF2::MultiCell($lbl_width, $lbl_min_height, $lbl_txt, 1, 'L', 0, 0, $x, $y, true);

        //     // increase the label count
        //     $current_col++;

        //     $total_labels_count++;

        //     if($current_col > $max_cols) {

        //         // reinitialize column count after max number of columns
        //         $current_col = 1;

        //         // increase the row count
        //         $current_row++;
        //     }
        // }

        // //Close and output PDF document
        // // put D instead of I to download it
        // PDF2::Output('labels.pdf', 'I');

    }
}
