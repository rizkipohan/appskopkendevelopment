<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemOut;
use App\ItemIn;
use App\Store;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Excel;
use App\Exports\ItemBalanceExport;
use App\Exports\ItemDetailExport;
use Auth;
use Illuminate\Support\Facades\Cookie;

class InventoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function stores()
    {
        $a = Store::all();
        return $a;
    }

    public function items()
    {
        // $a = Item::where('department_id',Auth::user()->department_id)->where('show','yes')->get();
        $a = Item::where('department_id',Auth::user()->department_id)->get();

        return $a;
    }

    public function indexHome(Request $request)
    {
        return view('inventory.index');
    }
    
    public function index($dpt)
    {
        $items = $this->items();
        // dd($items);
        return view('inventory/item/index',['items'=>$items]);
    }

    public function create()
    {
        return view('inventory/item/create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'qty' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        Item::create([
            'name' => $request->name,
            'qty' => 0,
            'price' => $request->price,
            'department_id' => Auth::user()->department_id
        ]);

        return redirect('inventory/item/index/'.Auth::user()->department_id)->with('success','berhasil menambah item!');
    }

    public function show(item $item)
    {
        //
    }

    public function edit(item $item)
    {
        
        return view('inventory.item.edit',['item'=>$item]);
    }

    public function update(Request $request, item $item)
    {
        Item::where('id',$item->id)->update([
            'name' => $request->name,
            // 'qty' => $request->qty,
            'price' => $request->price,
            // 'status' => $request->status
        ]);

        return redirect('inventory/item/index/'.Auth::user()->department_id)->with('success','berhasil mengubah item!');
    }

    public function destroy(item $item)
    {
        Item::where('id',$item->id)->delete();
        return redirect('inventory/item/index/'.Auth::user()->department_id);
    }

    public function delete(item $item)
    {
        return view('inventory.item.delete',['item'=>$item]);
    }

    public function itemOutIndex($dpt)
    {
        session()->forget('searchOut');
        $items = ItemOut::whereHas('item', function ($query) use ($dpt) {                                         
            $query->where('department_id', $dpt);
        })->get();
        return view('inventory/itemout/index',['items'=>$items]);
    }

    public function itemOutCreate()
    {
        $items = $this->items();
        $stores = $this->stores();
        return view('inventory/itemout/create',['items'=>$items,'stores'=>$stores]);
    }

    public function itemOutStore(Request $request)
    {
        
        ItemOut::create([
            'item_id' => $request->item,
            'store_id' => $request->store,
            'employee_name' => $request->employee_name,
            'qty' => $request->qty,
            'status' => $request->status,
        // ],[
            'serial_number' => $request->serial_number,
            'notes' => $request->notes,
            'date' => $request->date,
            'reason' => $request->reason
        ]);

        // $product = Item::findOrFail($request->item);
        // $product->qty -= $request->qty;
        // $product->update();
        
        return redirect('/inventory/itemout/index/'.Auth::user()->department_id);
    }

    public function itemOutEdit($item)
    {
        $itemout = ItemOut::where('id',$item)->first();
        $items = $this->items();
        $stores = $this->stores();
        // dd($itemout->store_id,$items,$stores);
        return view('/inventory/itemout/edit',compact('items','itemout','stores'));
    }

    public function itemOutUpdate($item, Request $request)
    {
        ItemOut::where('id',$item)->update([
            'item_id' => $request->item,
            'store_id' => $request->store,
            'qty' => $request->qty,
            'status' => $request->status,
            'serial_number' => $request->serial_number,
            'notes' => $request->notes,
            'date' => $request->date,
            'reason' => $request->reason,
            'employee_name' => $request->employee
        ]);
        // dd((object)session()->get('searchOut')->key);
        if(session()->exists('searchOut'))
        {
            $search = (object) session()->get('searchOut');
            return redirect('/inventory/itemout/search?key='.$search->key)->with(['success'=>'berhasil update data']);  
            session()->forget('searchOut');
        }else{
            return redirect('/inventory/itemout/index/'.Auth::user()->department_id)->with(['success'=>'berhasil update data']);
        } 
    }

    public function itemOutsearch(Request $request)
    {
        session(['searchOut' => $request->input()]);
        $key = $request->key;

        $items = ItemOut::
        orWhereHas('item', function ($query) use ($key) {                                         
            $query->where('name','like','%'.$key.'%');
        })->
        orWhereHas('store', function ($query) use ($key) {                                         
            $query->where('store_name','like','%'.$key.'%');
        })
        ->orWhere('employee_name','like','%'.$key.'%')
        ->get();
        return view('inventory/itemout/index',['items'=>$items]);
    }

    public function itemInIndex($dpt)
    {
        // session()->forget('searchIn');
        $items = ItemIn::whereHas('item', function ($query) use ($dpt) {                                         
            $query->where('department_id', $dpt);
        })->get();
        return view('/inventory/itemin/index',['items'=>$items]);
    }

    public function itemInCreate()
    {
        $items = $this->items();
        $stores = $this->stores();
        return view('inventory/itemin/create',['items'=>$items,'stores'=>$stores]);
    }

    public function itemInStore(Request $request)
    {
        ItemIn::create([
            'item_id' => $request->item,
            'qty' => $request->qty,
            'notes' => $request->notes,
            'date' => $request->date,
            'from' => $request->from,
            'vendor' => $request->vendor,
            'store_id' => $request->store,
            'po_number' => $request->po_number,
            'pr_number' => $request->pr_number
        ]); 
        // $product = Item::findOrFail($request->item);
        // $product->qty += $request->qty;
        // $product->update();
        
        return redirect('/inventory/itemin/index/'.Auth::user()->department_id);
    }

    public function itemInEdit($item)
    {
        $itemin = ItemIn::where('id',$item)->first();
        $items = $this->items();
        $stores = $this->stores();
        // dd($itemout->store_id,$items,$stores);
        return view('/inventory/itemin/edit',compact('items','itemin','stores'));
    }

    public function itemInUpdate($item, Request $request)
    {
        // dd($request->all());
        ItemIn::where('id',$item)->update([
            'item_id' => $request->item,
            'qty' => $request->qty,
            'notes' => $request->notes,
            'date' => $request->date,
            'from' => $request->from,
            'vendor' => $request->vendor,
            'store_id' => $request->store,
            'po_number' => $request->po_number,
            'pr_number' => $request->pr_number
        ]);

        if(session()->exists('searchIn'))
        {
            $search = (object) session()->get('searchOut');
            return redirect('/inventory/itemin/search?key='.$search->key)->with(['success'=>'berhasil update data']);  
            session()->forget('searchIn');
        }else{
            return redirect('/inventory/itemin/index/'.Auth::user()->department_id)->with(['success'=>'berhasil update data']);
        }
    }
 
    public function itemInDelete($item)
    {
        $x = ItemIn::findOrFail($item)->get();
        // dd($x->item_id);

        // $product = Item::findOrFail($x->item_id);
        // $product->qty -= $x->qty;
        // $product->update();

        ItemIn::where('id',$item)->first()->delete();
        
        return redirect('/inventory/itemin/index/'.Auth::user()->department_id);
    }

    public function itemOutDelete($item)
    {
        $x = ItemOut::where('id',$item)->get();
        // dd($x);

        // $product = Item::findOrFail($x->item_id);
        // $product->qty += $x->qty;
        // $product->update();

        ItemOut::where('id',$item)->first()->delete();
        
        return redirect('/inventory/itemout/index/'.Auth::user()->department_id);
    }

    public function itemInSearch(Request $request)
    {
        session(['searchIn' => $request->input()]);

        $key = $request->key;

        $items = ItemIn::
        // orWhere('from','like','%'.$key.'%')->
        orWhere('vendor','like','%'.$key.'%')->
        // orWhere('po_number','like','%'.$key.'%')->
        // orWhere('pr_number','like','%'.$key.'%')->
        orWhereHas('item', function ($query) use ($key) {                                         
            $query->where('name','like','%'.$key.'%');
        })->
        paginate(25);
        return view('inventory/itemin/index',['items'=>$items]);
    }

    public function reportItemBalanceIndex(Request $request)
    {
        if(isset($request->dept))
        {
            $dpt = $request->dept;
        }else{
            $dpt = Auth::user()->department_id;
        }
        // $items = Item::where('department_id', $dpt)->where('show','yes')->get();
        $items = Item::where('department_id', $dpt)->get();
        
        return view('/inventory/report/itembalance/index',compact('items','dpt'));
    }

    public function reportItemBalanceExport($dpt)
    {
        return (new ItemBalanceExport($dpt))->download('ReportItemBalance'.date('Ymd').'.xlsx');
    }

    // public function reportItemDetailIndex()
    // {
    //     $items = Item::all();
    //     return view('/inventory/report/itemdetail/index',compact('items'));
    // }

    public function reportItemDetailExport(Request $request,$item)
    {
        // $item = $request->item;
        // dd($item);
        // $x = Item::with([
        //     'itemin' => function($query) use ($item)
        //         {
        //             $query->where('item_id',$item); 
        //         }, 
        //     'itemout' => function($query) use ($item)
        //         {
        //             $query->where('item_id',$item); 
        //         }
        // ])->get();

        // dd($x[0]->itemin);
        return (new ItemDetailExport($item))->download('ReportItemDetail'.date('Ymd').'.xlsx');
    }

    public function cek($item)
    {
        $itemin = ItemIn::where('item_id',$item)->sum('qty');
        $itemout = ItemOut::where('item_id',$item)->sum('qty');
        $balance = $itemin - $itemout;
        dd($itemin,$itemout,$balance);
    }

    public function selectDepartment()
    {
        $dept = Department::all();
        return view('/inventory/report/selectDept',compact('dept'));
    }
}
