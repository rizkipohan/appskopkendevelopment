<?php

namespace App\Http\Controllers;

use App\Store;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $stores = Store::all();
        return view('master.store.index',['stores'=>$stores]);
    }

    
    public function create()
    {
        // $providers = Provider::all();
        return view('master.store.create');
    }

    
    public function store(Request $request)
    {
        // $validator = Validator::make($request->all(), [
        //     'store_name' => 'required|unique:stores,store_name',
        //     'store_code' => 'required|unique:stores,store_code',
        // ]);

        // if ($validator->fails()) {
        //     return redirect()->back()->withInput()->withErrors($validator);
        // }

        Store::create([
            'store_code' => $request->store_code,
        // ],[
            'store_name' => $request->store_name,
            'store_address' => $request->store_address,
            'store_status' => $request->store_status,
            'store_phone' => $request->store_phone,
            'store_email' => $request->store_email,
            'tax_number' => $request->tax_number,
            'open_date' => $request->open_date,
            'connection_status' => $request->connection_status,
            // 'online_date' => $request->online_date,
            // 'active_date' => $request->active_date,
            'provider' => $request->provider,
            'sid' => $request->sid,
            'mikrotik_status' => $request->mikrotik_status,
            'ip_local' => $request->ip_local,
            'ip_wan' => $request->ip_wan,
            'ip_gateway_wan' => $request->ip_gateway_wan,
            'ip_dns' => $request->ip_dns,
            'ip_vpn' => $request->ip_vpn,
        ]);
        
        return redirect('master/store/index');

    }

   
    public function search(Request $request,Store $store)
    {
        $stores = Store::where('store_name','LIKE','%'.$request->input('search').'%')
        ->orWhere('store_code','LIKE','%'.$request->input('search').'%')->paginate(25);
        // dd($stores);
        return view('master.store.index',['stores'=>$stores]);
    }

    
    public function edit(Store $store)
    {
        // $providers = Provider::all();
        return view('master.store.edit',['store'=>$store]);
    }

    
    public function update(Request $request, Store $store)
    {
        $validator = Validator::make($request->all(), [
            // 'store_name' => 'required|unique:stores,store_name',
            // 'store_code' => 'required|unique:stores,store_code',
            // 'sid'=> 'unique:stores,sid',
            // 'tax_number' => 'unique:stores,tax_number'
        ]); 

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        Store::where('id',$store->id)
        ->update([
            'store_code' => $request->store_code,
            'store_name' => $request->store_name,
            'store_status' => $request->store_status,
            'store_address' => $request->store_address,
            'store_phone' => $request->store_phone,
            'store_email' => $request->store_email,
            'tax_number' => $request->tax_number,
            'open_date' => $request->open_date,
            'connection_status' => $request->connection_status,
            // 'online_date' => $request->online_date,
            // 'active_date' => $request->active_date,
            'provider' => $request->provider,
            'sid' => $request->sid,
            'mikrotik_status' => $request->mikrotik_status,
            'ip_local' => $request->ip_local,
            'ip_wan' => $request->ip_wan,
            'ip_gateway_wan' => $request->ip_gateway_wan,
            'ip_dns' => $request->ip_dns,
            'ip_vpn' => $request->ip_vpn,
        ]);
        return redirect('master/store/index')->with('success','Berhasil mengubah data !');
    }

    
    public function destroy(Store $store)
    {
        Store::where('id',$store->id)->delete();
        return redirect('master/store/index');
    }

    public function delete(Store $store)
    {
        return view('master.store.delete',['store'=>$store]);
    }
}
