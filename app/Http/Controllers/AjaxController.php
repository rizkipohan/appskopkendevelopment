<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Department;

class AjaxController extends Controller
{
    public function dept()
    {
        $dept = Department::all();
        return response()->json($dept);
    }

}