<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bast;
use App\Store;

class BastController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getStore()
    {
        $result = Store::all();
        return $result;
    }

    public function getBast()
    {
       $result = Bast::all();
       return $result;
    }

    public function index()
    {
        return view('bast.index',['bast'=>$this->getBast()]);
    }

    public function create()
    {
        return view('bast.create',['stores'=>$this->getStore()]);
    }

    public function store(Request $request)
    {
        // dd('lewat sini');
        $this->validate($request,['bast' => 'required|mimes:doc,docx,pdf']);
        $storename = Store::findOrFail($request->store_id);
        // dd($storename->store_name);

        $files = $request->file('bast');
        $destinationPath = public_path('bast/'); 
        $bastname = $storename->store_name.'-'.date('YmdHis').'.'.$files->extension();
        $files->move($destinationPath, $bastname);

        Bast::create([
            'store_id' => $request->store_id,
            'path'=> url('public/bast').'/'.$bastname,
            'filename'=>$bastname
        ]);

        return redirect('bast/');
    }
}
