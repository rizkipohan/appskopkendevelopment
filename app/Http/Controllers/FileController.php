<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\File;
use App\Folder;

class FileController extends Controller
{
    public function indexFolder($id)
    {
        $files = File::where('folder_id',$id)->paginate(25);
        $folder = Folder::findOrFail($id);
        return view('/lnd/file/index',compact('files','folder'));
    }

    public function index()
    {
        $files = File::paginate(25);
        // dd($files);
        return view('/lnd/file/index',compact('files'));
    }

    public function create()
    {
        $folders=Folder::all();
        return view('/lnd/file/create',compact('folders'));
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            // 'file' => 'mimes:pdf,jpeg,png,jpg,gif,mp4,mpeg4,mov,flv,avi,wmv',
            // 'file' => 'mimes:doc,docx,xls,xlsx,txt,csv,ppt,pptx,pdf,jpeg,png,jpg,gif,mp4,mpeg4,mov,flv,avi,wmv',
        ]);
        // dd($request->file('file')->getMimeType());

        $files = $request->file('file');
        $destinationPath = public_path('document'); 
        $format = $files->extension();
        $mime = $files->getMimeType();
        $fileName = $request->rename.'.'.$format;
        $fullPath = $destinationPath.'/'.$fileName;
        $files->move($destinationPath, $fileName);
        // dd($files);
        
        File::create([
            'name' => $fileName,
            'format' => $format,
            'mime_type' => $mime,
            'full_path' => $fullPath,
            'folder_id' => $request->folder
        ]);

        return redirect('/folder/file/index/'.$request->folder)->with(['success'=>'File Uploaded!']);
    }

    public function show($id)
    {
        $files = File::where('id',$id)->first();
        return view('/lnd/file/show',compact('files'));
    }
}