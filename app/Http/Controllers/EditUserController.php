<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Department;

class EditUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit(User $user)
    {   
        $user = Auth::user();
        $dpts = Department::all();
        return view('auth.edit', compact('user','dpts'));
    }

    public function update(User $user, Request $request)
    { 
        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->department_id = $request->input('department');
        // $user->password = bcrypt(request('password'));
        // $user->save();

        $cek = User::where('id',$user->id)
        ->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'department_id' => $request->input('department')
        ]);
        
        if($cek){
            return back()->with('success','berhasil edit profile!');
        }else{
            return back()->with('error','terjadi kesalahan!');
        }
    }
}
