<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';
    
    protected $fillable  = [
        'name','format','full_path','mime_type','folder_id'
    ];

    public function folder()
    {
        return $this->belongsTo('App\Folder');
    }
}
