<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreProperties extends Model
{
    protected $table = 'store_properties';

    protected $guarded = [];

    public function store()
    {
        return $this->belongsTo('App\Store');
    }
}
