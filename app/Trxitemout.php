<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trxitemout extends Model
{
    use SoftDeletes;

    protected $table = 'trx_item_out';

    protected $guarded = [];

    public function itemdetail()
    {
        return $this->belongsTo('App\ItemDetail','item_detail_id');
    }

    public function location()
    {
        return $this->belongsTo('App\Store','location_id');
    }

}
