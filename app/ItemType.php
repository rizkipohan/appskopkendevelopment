<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemType extends Model
{
    use SoftDeletes;

    protected $table = 'mst_item_type';

    protected $guarded = [];

    public function itemname()
    {
        return $this->hasMany('App\ItemName');
    }
}
