<?php

namespace App\Exports;

use App\Item;
use Auth;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;


class ItemBalanceExport implements FromView
{
    use Exportable;

    public function __construct(int $dpt)
    {
        $this->dpt = $dpt;
        return $this->dpt; 
    }
    
    public function view(): View
    {
        // $items = Item::all();
        return view('inventory.report.itembalance.itembalanceexport',[
            'items'=>Item::where('department_id',$this->dpt)->get()
        ]);
    }
}
