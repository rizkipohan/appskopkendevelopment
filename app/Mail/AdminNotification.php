<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $admin, $reqs, $items;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin,$reqs,$items)
    {
        $this->admin = $admin;
        $this->reqs = $reqs;
        $this->items = $items;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.AdminNotification')
        ->from('it.servicedeskprinter@kopikenangan.com','IT Portal')
        ->subject('You have approve an item request');
    }
}
