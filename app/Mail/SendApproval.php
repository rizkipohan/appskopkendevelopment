<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendApproval extends Mailable
{
    use Queueable, SerializesModels;

    public $user, $reqs, $items;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $reqs, $items)
    {
        $this->user = $user;
        $this->reqs = $reqs;
        $this->items = $items;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.SendApproval')
        ->from('it.servicedesk@kopikenangan.com','IT Portal')
        ->subject('Item Request Has Been Approved');
    }
}
