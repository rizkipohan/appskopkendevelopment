<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class YourRequestItem extends Mailable
{
    use Queueable, SerializesModels;

    public $data,$items,$user,$id;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$items,$user,$id)
    {
        $this->id = $id;
        $this->data = $data;
        $this->items = $items;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.YourRequestItem')
        ->subject('Your Request Item');
    }
}
