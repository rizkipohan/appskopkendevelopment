@component('mail::message')

Dear {{$user->name}}, <br>
Berikut ini adalah item request anda:

@component('mail::table')
<table>
   <tr>
       <td>Toko</td>
       <td>:</td>
       <td>{{$data->store_name}}</td>
   </tr>
   <tr>
       <td>Item</td>
       <td colspan="2">:</td>
   </tr>
</table>
<table style="text-align:center;">
    <tr>
        <th>no</th>
        <th>nama</th>
        <th>qty</th>
        <th>serial number</th>
    </tr>
    @foreach($items as $key => $item)
     <tr>
         <td>{{$key+1}}</td>
         <td>{{$item['item_name']}}</td>
         <td>{{$item['qty']}}</td>
         <td>{{$item['serial_number']}}</td>
     </tr>
    @endforeach
</table>

@endcomponent

@endcomponent
