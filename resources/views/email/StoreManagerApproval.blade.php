@component('mail::message')

Dear PIC toko {{$reqs->store_name}},

Berikut ini adalah hasil pengecekan perangkat IT yang dilakukan oleh {{Auth::user()->name}}

@component('mail::table')

<table>
    <tr>
        <td>Store Name</td>
        <td>:</td>
        <td>{{$reqs->store_name}}</td>
    </tr>
    <tr>
        <td>Store Code</td>
        <td>:</td>
        <td>{{$reqs->store_code}}</td>
    </tr>
    <tr>
        <td>Open Date</td>
        <td>:</td>
        <td>{{$reqs->open_date}}</td>
    </tr>
    <tr>
        <td>Store Email</td>
        <td>:</td>
        <td>{{$reqs->email}}</td>
    </tr>
    <tr>
        <td>Internet Status</td>
        <td>:</td>
        <td>{{$reqs->internet}}</td>
    </tr>
    <tr>
        <td>Internet Provider</td>
        <td>:</td>
        <td>{{$reqs->isp}}</td>
    </tr>
    <tr>
        <td>SID</td>
        <td>:</td>
        <td>{{$reqs->sid}}</td>
    </tr>
    <tr>
        <td>IP Public</td>
        <td>:</td>
        <td>{{$reqs->ip_public}}</td>
    </tr>
    <tr>
        <td>IP Reserved</td>
        <td>:</td>
        <td>{{$reqs->ip_reserved}}</td>
    </tr>
    <tr>
        <td>CCTV Record</td>
        <td>:</td>
        <td>{{$reqs->cctv_recording}}</td>
    </tr>
    <tr>
        <td>CCTV cloud id</td>
        <td>:</td>
        <td>{{$reqs->cctv_cloud_id}}</td>
    </tr>
    <tr>
        <td>CCTV camera qty </td>
        <td>:</td>
        <td>{{$reqs->cctv_cam_qty}}</td>
    </tr>
    <tr>
        <td>CCTV Streaming Status</td>
        <td>:</td>
        <td>{{$reqs->cctv_streaming}}</td>
    </tr>
    <tr>
        <td>CCTV Regional Status</td>
        <td>:</td>
        <td>{{$reqs->cctv_regional}}</td>
    </tr>
    <tr>
        <td>PoS</td>
        <td>:</td>
        <td>{{$reqs->pos}}</td>
    </tr>
    <tr>
        <td>Pos WiFi</td>
        <td>:</td>
        <td>{{$reqs->pos_wifi}}</td>
    </tr>
    <tr>
        <td>WiFi Extended Cable</td>
        <td>:</td>
        <td>{{$reqs->wifi_extended_cable}}</td>
    </tr>
    <tr>
        <td>Printer Label</td>
        <td>:</td>
        <td>{{$reqs->printer_label}}</td>
    </tr>
    <tr>
        <td>Tablet</td>
        <td>:</td>
        <td>{{$reqs->tablet}}</td>
    </tr>
    <tr>
        <td>Laptop</td>
        <td>:</td>
        <td>{{$reqs->laptop}}</td>
    </tr>
    <tr>
        <td>T-hub Type</td>
        <td>:</td>
        <td>{{$reqs->t_hub}}</td>
    </tr>
    <tr>
        <td>PoS Trainer</td>
        <td>:</td>
        <td>{{$reqs->pos_trainer}}</td>
    </tr>
    <tr>
        <td>USB LCD Content</td>
        <td>:</td>
        <td>{{$reqs->usb_lcd_content}}</td>
    </tr>
    <tr>
        <td>Code in Quinos Creating Order</td>
        <td>:</td>
        <td>{{$reqs->code_creating_order}}</td>
    </tr>
    <tr>
        <td>Create Offline Order</td>
        <td>:</td>
        <td>{{$reqs->create_offline_order}}</td>
    </tr>
    <tr>
        <td>Check Order Appear in Apps</td>
        <td>:</td>
        <td>{{$reqs->check_order_appear_in_apps}}</td>
    </tr>
    <tr>
        <td>Scan QR Code create order</td>
        <td>:</td>
        <td>{{$reqs->scan_qr_code_create_order}}</td>
    </tr>
    <tr>
        <td>Data Product and Promo</td>
        <td>:</td>
        <td>{{$reqs->data_product_promo}}</td>
    </tr>
    <tr>
        <td>Employee Register</td>
        <td>:</td>
        <td>{{$reqs->employee_register}}</td>
    </tr>
    <tr>
        <td>Employee Training</td>
        <td>:</td>
        <td>{{$reqs->employee_training}}</td>
    </tr>
</table>

@endcomponent


@endcomponent
