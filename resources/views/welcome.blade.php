<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>IT Portal</title>

        <link rel="icon" href="https://kopikenangan.co.id/wp-content/uploads/2019/05/cropped-ms-icon-310x310-32x32.png" sizes="32x32" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('user/edit/'.Auth::user()->id) }}" class="dropdown-item">
                            Edit Your Profile
                        </a>
                        <a class="btn btn-danger" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    IT Portal
                </div>

                <div class="links">
                    @if (Auth::user()->department_id == "2")
                        <a href="{{url('/master/store/index')}}">Master Store</a>
                        <a href="{{url('/nso/index')}}">NSO Approve</a>
                        <a href="{{url('/requestitem/create')}}">Request Item</a>
                        <a href="{{url('/itasset')}}">IT Asset</a>
                        <a href="{{url('/storeprops/create')}}">Store Properties Check</a>
                        <a href="{{url('/inventory')}}">Inventory Apps</a>
                        {{-- <a href="{{url('/tickets')}}">Ticket Apps</a> --}}
                        {{-- <a href="{{url('/bast')}}"> BAST </a> --}}
                    @endif

                    @if (Auth::user()->department_id == "9")
                        <a href="{{url('/nso/index')}}">NSO Approve</a>
                    @endif
 
                    {{-- @if (Auth::user()->department_id == "2" || Auth::user()->department_id == "7" || Auth::user()->department_id == "8")
                        <a href="{{url('/folder/index')}}">L&D Portal</a>
                    @endif --}}

                    {{-- @if (Auth::user()->department_id == "2" || Auth::user()->department_id == "5" || Auth::user()->department_id == "6")
                        <a href="{{url('/inventory')}}">Inventory Apps</a>
                    @endif --}}

                    {{-- <a href="{{url('/itdocs')}}">IT Document</a> --}}

                </div>
            </div>
        </div>
    </body>
</html>
