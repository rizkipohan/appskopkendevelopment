@extends('master.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center"><h3>are you sure to delete this data?</h3></div>

                <div class="card-body">
                <form action="{{ url('master/store/destroy/'.$store->id) }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-danger btn-block">Yes</button>
                    <a class="btn btn-success btn-block" href="{{ url('master/store/index') }}">No</a>
                </div>
            </div>

            
                <div class="form-group">
                    <label for="">Store Name</label>
                    <input type="text" class="form-control" name="store_name" id="" value="{{$store->store_name}}" readonly>
                </div>
                <div class="form-group">
                    <label for="">Store Code</label>
                    <input type="text" class="form-control" name="store_code" id="" value="{{$store->store_code}}" readonly>
                </div>
                <div class="form-group">
                    <label for="">Store Address</label>
                    <textarea class="form-control" name="store_address" id="" rows="3" readonly>{{$store->store_address}}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Store Email</label>
                    <input type="email" class="form-control" name="store_email" id="" value="{{$store->store_email}}" readonly>
                </div>
                <div class="form-group">
                    <label for="">Store Phone</label>
                    <input type="text" name="store_phone" id="" class="form-control" value="{{$store->store_phone}}" readonly>
                </div>
                
                
            </form>
        </div>
    </div>
</div>
@endsection