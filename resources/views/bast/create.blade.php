@extends('bast.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Upload BAST File</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-10">
            <form action="{{url('/bast/store')}}" method="POST" autocomplete="off" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-9">
                        <label for="">Select Store</label>
                        <select name="store_id" class="custom-select" id="store_id" required>
                            <option value="" selected></option>
                            @foreach ($stores as $store)
                                <option value="{{$store->id}}">{{$store->store_name}} ({{$store->store_code}})</option>
                            @endforeach
                            <script>
                                $("#store_id").select2({
                                    selectOnClose: true,
                                    theme: "bootstrap"
                                });
                            </script>
                        </select>
                    </div>
                    <div class="form-group col">
                        <br>
                        <a class="btn btn-info btn-block" href="{{url('master/store/create')}}">Add New Store</a>  
                    </div>          
                </div>
                <div class="form-group">
                    <label for="">Upload File:</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="bast" name="bast">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                        <script>
                            $('#bast').on('change',function(){
                                //get the file name
                                var fileName = $(this).val();
                                //replace the "Choose a file" label
                                $(this).next('.custom-file-label').html(fileName);
                            })
                        </script>
                    </div>                    
                </div>
                <div class="form-group justify-content-center">
                    <button type="submit" class="btn btn-success btn-block">Submit</button>
                </div>
            </form>
    </div>
</div>

@endsection
