@extends('bast.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            
            <div class="d-flex justify-content-center">
                <a name="" id="" class="btn btn-success my-2" href="{{url('/#')}}" role="button">Add</a>
            </div>

            <table class="table table-striped table-hover" id="table">
                <thead>
                    <tr class="text-center">
                        <th>#</th>
                        <th>Store Name</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($reqitem)==0)
                    <tr class="text-center"><td colspan="100">No Data</td></tr>
                    @else
                        @foreach ($reqitem as $key => $item)
                        <tr class="text-center">
                            <td>{{$key+1}}</td>
                            <td></td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#table').DataTable();
    });

</script>
@endsection
