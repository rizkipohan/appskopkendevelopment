<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>NSO Approval PDF</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script type="text/javascript" src="http://keith-wood.name/js/jquery.signature.js"></script>

    <link rel="stylesheet" type="text/css" href="http://keith-wood.name/css/jquery.signature.css">
  
</head>
<style>
    .page-break {
        page-break-before: always;
    }
</style>
<body class="">
    <div class="row justify-content-center mb-0">
        <div class="col-md-12">
            <table class="table text-center">
            <tbody>
                <tr>
                    <td>
                        <img src="https://kopikenangan.com/wp-content/uploads/2018/09/Logo-Kopi-Kenangan-baru-vertical-2-line-trans.png" class="" style="width: 100px; height: 100px;" alt="">	
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
    </div> 

    Store Name: <b>{{$stores->store_name}}</b> <br><br> 
    Open Date: <b>{{$stores->open_date}}</b> <br> <br>
    Address: <b>{{$stores->store_address}}</b> <br><br>
    Email: <b>{{$stores->store_email}}</b> <br> <br>
    Phone: <b>{{$stores->store_phone}}</b> <br> <br>
    Provider: <b>{{$stores->provider}}</b> <br> <br>
    CCTV Layout: <br><br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            
            @isset($stores->img)
            <img src="{{$stores->img}}" class="img-fluid" alt="">
            @endisset
                    
        </div>
    </div>

    <div class="page-break"></div>

    IT Equipment: <br> <br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover table-bordered text-center">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>Name</td>
                        <td>Quantity</td>
                        <td>Serial Number</td>
                        <td>Available</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>POS</td>
                        <td>{{$stores->pos_quantity}}</td>
                        <td>{{$stores->pos_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="pos_check" id="" value="1" @isset($stores->pos_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Drawer</td>
                        <td>{{$stores->drawer_quantity}}</td>
                        <td>{{$stores->drawer_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="drawer_check" id="" value="1" @isset($stores->drawer_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Laptop</td>
                        <td>{{$stores->laptop_quantity}}</td>
                        <td>{{$stores->laptop_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="laptop_check" id="" value="1"  @isset($stores->laptop_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Scanner</td>
                        <td>{{$stores->scanner_quantity}}</td>
                        <td>{{$stores->scanner_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="scanner_check" id="" value="1"  @isset($stores->scanner_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Tablet</td>
                        <td>{{$stores->tablet_quantity}}</td>
                        <td>{{$stores->tablet_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="tablet_check" id="" value="1"  @isset($stores->tablet_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Handphone</td>
                        <td>{{$stores->hp_quantity}}</td>
                        <td>{{$stores->hp_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="hp_check" id="" value="1" @isset($stores->hp_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Printer Thermal</td>
                        <td>{{$stores->thermal_quantity}}</td>
                        <td>{{$stores->thermal_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="thermal_check" id="" value="1" @isset($stores->thermal_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Mikrotik</td>
                        <td>{{$stores->mikrotik_quantity}}</td>
                        <td>{{$stores->mikrotik_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="mikrotik_check" id="" value="1" @isset($stores->mikrotik_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Camera</td>
                        <td>{{$stores->camera_quantity}}</td>
                        <td>{{$stores->camera_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="camera_check" id="" value="1" @isset($stores->camera_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>DVR</td>
                        <td>{{$stores->dvr_quantity}}</td>
                        <td>{{$stores->dvr_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="dvr_check" id="" value="1" @isset($stores->dvr_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Printer Label (Bixolon)</td>
                        <td>{{$stores->label_quantity}}</td>
                        <td>{{$stores->label_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="label_check" id="" value="1" @isset($stores->label_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>T-Hub</td>
                        <td>{{$stores->thub_quantity}}</td>
                        <td>{{$stores->thub_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="thub_check" id="" value="1" @isset($stores->thub_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>USB Extension</td>
                        <td>{{$stores->ext_quantity}}</td>
                        <td>{{$stores->ext_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="ext_check" id="" value="1" @isset($stores->ext_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Flashdisk</td>
                        <td>{{$stores->fd_quantity}}</td>
                        <td>{{$stores->fd_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="fd_check" id="" value="1" @isset($stores->fd_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Stand Tablet</td>
                        <td>{{$stores->stand_quantity}}</td>
                        <td>{{$stores->stand_sn}}</td>
                        <td>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="stand_check" id="" value="1" @isset($stores->stand_check) checked @endisset>
                                </label>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="page-break"></div>

    Approved by: <br> <br>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table table-hover table-borderless text-center">
                <thead>
                    <tr>
                        <td>Store Manager</td>
                        <td>IT Project SPV</td>
                        {{-- <td>IT Support SPV</td> --}}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <img src="{{ $stores->sig }}">
                            <br>
                            {{$stores->sm}}
                        </td>
                        <td>
                            <img src="{{ $stores->sig1 }}">
                            <br>
                            {{$stores->spv}}
                        </td>
                        {{-- <td><img src="{{ $stores->sig2 }}"></td> --}}
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>

</body>
</html>