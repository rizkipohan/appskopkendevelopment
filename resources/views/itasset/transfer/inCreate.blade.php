@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Item List</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <form action="{{route('store_trfin')}}" method="POST" autocomplete="off">
                @csrf

                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Item Name</th>
                                <th>IT Asset Code</th>
                                <th>fam code</th>
                                <th>Serial Number</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            @foreach($items as $key => $item )
                            <input type="hidden" name="location[]" value="{{$item->location_id}}">
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$item->itemdetail->itemtype->name." ".$item->itemdetail->itemname->name}}</td>
                                <td>{{$item->itemdetail->it_asset_code.$item->itemdetail->increment_id}}</td>
                                <td>{{$item->itemdetail->fam_code}}</td>
                                <td>{{$item->itemdetail->serial_number}}</td>
                                <td><input type="checkbox" name="item[]" id="" value="{{$item->itemdetail->id}}"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                
                <div class="form-group mb-3">
                    <button type="submit" class="btn btn-success">Create Item In</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection