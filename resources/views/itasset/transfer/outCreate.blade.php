@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Transfer Out</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="{{route('store_trfout')}}" method="POST" autocomplete="off">
                @csrf

                <div class="form-group mb-3">
                    <label for="">Destination:</label>
                    <select name="location" class="form-control" id="location"></select>
                </div>
              
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Select Item</th>
                            </tr>
                        </thead>
                        <tbody id="table">
                           
                        </tbody>
                    </table>
                </div>

                <div class="form-group text-center">
                    <button type="button" class="btn btn-success mb-2" id="add">Add New Item</button><br>
                    <button type="button" class="btn btn-danger" id="delete">Remove Last Item</button>
                </div>

                <div class="form-group d-grid mt-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        //JQUERY ITEM
        var angka = 1;
        $("#add").click(function(){
            $('#table').append(`
                <tr>
                    <td>`+angka+`</td>
                    <td>
                        <select name="item[]" class="form-select form-select-sm" id="item_id[`+angka+`]" required>
                            <option value="">-</option>
                            `+"{!!$opt!!}"+`
                        </select>
                    </td>
                </tr>
            `);
            angka++;
        });

        $("#delete").click(function(){
            $('#table tr:last').remove();
            if(angka>1){
                angka--;
            }
        });

       
        //JQUERY LOCATION
        $.get("{{url('/itasset/jq/getlocation')}}",
            function(data){
                $("#location").append(data);
        });
        
        $("select").select2({ selectOnClose: true, theme: "bootstrap"});

    });
</script>

@endsection
