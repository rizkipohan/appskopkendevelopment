@extends('itasset.app')

@section('content')
<div class="container">



    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Select Store</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4">
            <form action="{{url('/itasset/checkout/showbookeditem')}}" method="POST" autocomplete="off">
                @csrf

                <div class="form-group mb-3">
                    <label for="">Select Location</label>
                    <select name="location" class="form-control" id="location" required>
                        <option value=""></option>
                    </select>
                </div>

                <div class="form-group d-grid">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script>
    $.get("{{url('/itasset/jq/book/getlocationbook')}}",
        function(data){
            $("#location").append(data);
    });
    $("#location").select2({ selectOnClose: true, theme: "bootstrap"});
</script>

@endsection
