@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Item Data</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10">
           <div class="table-responsive">
            <table class="table table-striped text-center" id="data-table">
                <thead>
                    <th>#</th>

                </thead>
                <tbody>
                </tbody>
            </table>
           </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        var oTable = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url("#") }}'
            },
            columns: [
            // {data: 'DT_RowIndex', orderable: false, searchable: false},
            // {data: 'pr_number', name: 'pr_number'},
            ]
        });
    });
</script>

@endsection
