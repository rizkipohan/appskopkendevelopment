@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Booking Item</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="{{url('/itasset/booking/bookitem')}}" method="POST" autocomplete="off">
                @csrf

                <div class="form-group mb-3">
                    <label for="">Location</label>
                    <select name="location" class="form-control" id="location"></select>
                </div>

                <div class="form-group mb-3">
                    <label for="">Class</label>
                    <select name="class" class="form-control" id="class"></select>
                </div>

                <div class="form-group mb-3">
                    <label for="">Dept</label>
                    <select name="dept" class="form-control" id="dept"></select>
                </div>

                <div class="form-group mb-3">
                    <label for="">Designation</label>
                    <select name="designation" class="form-control" id="designation"></select>
                </div>

                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Select Item</th>
                            </tr>
                        </thead>
                        <tbody id="table">
                            {{-- <tr>
                                <td>1.</td>
                                <td>
                                    <select name="item_id[0]" class="form-select" id="" required>
                                        <option value="">-</option>
                                    </select>
                                </td>
                            </tr> --}}
                        </tbody>
                    </table>
                </div>

                <div class="form-group text-center">
                    <button type="button" class="btn btn-success mb-2" id="add">Add New Item</button><br>
                    <button type="button" class="btn btn-danger" id="delete">Remove Last Item</button>
                </div>

                {{-- <div class="form-group mb-3">
                    <label for="">Select Item</label>
                    <select name="item" class="form-control" id="item" required>
                        <option value=""></option>
                    </select>
                    <input type="hidden" name="itemtype" id="itemtype">
                    <input type="hidden" name="itemname" id="itemname">
                </div> --}}

                <div class="form-group d-grid mt-3">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        //JQUERY ITEM
        var angka = 1;
        $("#add").click(function(){
            $('#table').append(`
                <tr>
                    <td>`+angka+`</td>
                    <td>
                        <select name="item_id[`+(angka-1)+`]" class="form-select form-select-sm" required>
                            <option value="">-</option>
                            `+"{!!$opt!!}"+`
                        </select>
                    </td>
                </tr>
            `);
            angka++;
        });

        $("#delete").click(function(){
            $('#table tr:last').remove();
            if(angka>1){
                angka--;
            }
        });

        //JQUERY CLASS
        $.get("{{url('/itasset/jq/book/getclassbook')}}",
            function(data){
                $("#class").append(data);
        });
        $("#class").select2({ selectOnClose: true, theme: "bootstrap"});

        //JQUERY DEPT
        $.get("{{url('/itasset/jq/book/getdeptbook')}}",
            function(data){
                $("#dept").append(data);
        }).done(function(){
            $("#dept option:contains('KK Op')").attr("selected", "selected");
        });
        $("#dept").select2({ selectOnClose: true, theme: "bootstrap"});

        //JQUERY DESIGNATION
        $.get("{{url('/itasset/jq/book/getdesignationbook')}}",
            function(data){
                $("#designation").append(data);
        });
        $("#designation").select2({ selectOnClose: true, theme: "bootstrap"});

        //JQUERY LOCATION
        $.get("{{url('/itasset/jq/book/getlocationbook')}}",
            function(data){
                $("#location").append(data);
        });
        $("#location").select2({ selectOnClose: true, theme: "bootstrap"});


    });
</script>

@endsection
