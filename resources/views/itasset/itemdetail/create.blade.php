@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Item In</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="{{url('/itasset/itemdetail/store')}}" method="POST" autocomplete="off">
                @csrf

                {{-- PR NUMBER --}}
                <div class="form-group mb-3">
                    <label for="">PR Number</label>
                    <input type="text" name="pr_number" class="form-control">
                </div>
                {{--  --}}

                {{-- ITEM TYPE --}}
                <div class="form-group mb-3">
                    <label>Item Type</label>
                    <div class="row">
                        <div class="col-md-10">
                            <select name="itemtype" id="itemtype" class="form-select" required>
                            </select>
                        </div>
                        <div class="col-md d-grid gap-2">
                            {{-- <a class="btn btn-success btn-block" data-bs-toggle="modal" data-bs-target="#modalitemtype"> --}}
                            <a href="{{url('itasset/master/itemtype/index')}}" class="btn btn-success">
                                Add
                            </a>
                        </div>
                    </div>
                </div>
                {{--  --}}

                {{-- ITEM Name --}}
                <div class="form-group mb-3">
                    <label>Item Name</label>
                    <div class="row">
                        <div class="col-md-10">
                            <select name="itemname" id="itemname" class="form-select" required>
                            </select>
                        </div>
                        <div class="col-md d-grid gap-2">
                            {{-- <a class="btn btn-success btn-block" data-bs-toggle="modal" data-bs-target="#modalitemtype"> --}}
                            <a href="{{url('itasset/master/itemname/index')}}" class="btn btn-success">
                                Add
                            </a>
                        </div>
                    </div>
                </div>
                {{--  --}}

                {{-- Vendor --}}
                <div class="form-group mb-3">
                    <label>Vendor</label>
                    <div class="row">
                        <div class="col-md-10">
                            <select name="vendor" id="vendor" class="form-select" required>
                            </select>
                        </div>
                        <div class="col-md d-grid gap-2">
                            {{-- <a class="btn btn-success btn-block" data-bs-toggle="modal" data-bs-target="#modalitemtype"> --}}
                            <a href="{{url('itasset/master/vendor/index')}}" class="btn btn-success">
                                Add
                            </a>
                        </div>
                    </div>
                </div>
                {{--  --}}

                {{-- QUANTITY --}}
                <div class="form-group mb-3">
                    <label for="">Quantity</label>
                    <div class="row">
                        <div class="col-md">
                           <input type="number" name="qty" class="form-control text-center" id="qty" min="1" value="1" >
                        </div>
                        <div class="col-md d-grid gap-2">
                            <button type="button" class="btn btn-success" id="gnrt">Create Row</button>
                        </div>
                    </div>
                </div>
                {{--  --}}

                <div class="table-responsive">
                    <table class="table table-secondary table-striped text-center">
                        <thead>
                            <tr>
                                <th>#</th>
                                {{-- <th>Serial Number</th> --}}
                                <th>FAM Code</th>
                            </tr>
                        </thead>
                        <tbody id="table" class="text-center">
                            <tr>
                                <td>1</td>
                                {{-- <td><input type="text" name="serial_number[0]" class="form-control"></td> --}}
                                <td><input type="text" name="fam_code[0]" class="form-control"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="form-group d-grid">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalitemtype" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item Type</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <input type="text" name="itemtypename" class="form-control" id="itemtypename">
                </div>
                <div class="form-group d-grid mt-2">
                    <button type="button" id="btnadditemtype" class="btn btn-success">
                        Add
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalitemname" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item Name</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Select Item Type</label>
                    <select name="" class="custom-select" id="itemType2">
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Item Name</label>
                    <input type="text" name="" class="form-control" id="itemName">
                </div>
                <div class="form-group d-grid mt-2">
                    <button type="button" id="additemname" class="btn btn-success">
                        Add
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var itemName = $('#itemname');
        var itemType = $('#itemtype');
        var vendor = $('#vendor');

        //============================= JQUERY UNTUK ITEM TYPE ===============================
        rfshItemType();
        function rfshItemType(){
            itemType.empty();
            $.get('{{url('/itasset/jq/getitemtype')}}',
            function(data) {
                itemType.append(data);
                itemName.empty();
                var value = itemType.find(":selected").val();
                $.post("{{url('/itasset/jq/getitemnamebytype')}}",
                    {
                        id: value
                    },
                    function(data) {
                        itemName.append(data);
                    }
                );
            });
            itemType.select2({selectOnClose: true, theme: "bootstrap"});
            itemName.select2({selectOnClose: true, theme: "bootstrap"});
        }

        $('#btnadditemtype').click(function(){
            var itemTypeName = $('#itemtypename');
            $.post("{{url('/itasset/jq/additemtype')}}",
            {
                name: itemTypeName.val()
            },
            function(data, status){
                rfshItemType();
                itemTypeName.empty();
                alert("Status: " + status);
            });
        });
        //====================================================================================

        //============================= JQUERY UNTUK ITEM NAME ===============================
        itemType.on('change', function(){
            itemName.empty();
            var value = $(this).find(":selected").val();
            // console.log(value);
            $.post("{{url('/itasset/jq/getitemnamebytype')}}",
            {
                id: value
            },
            function(data) {
                // console.log(value);
                itemName.append(data);
            });
        });
        //====================================================================================

        //============================= JQUERY UNTUK Vendor ==================================
        $.get('{{url('/itasset/jq/getvendor')}}',
            function(data) {
                vendor.append(data);
                // itemType2.append(data);
            }
        );
        vendor.select2({ selectOnClose: true, theme: "bootstrap"});
        //====================================================================================

        //========================= JQUERY UNTUK QTY SERIAL NUMBER ===========================

        var x = $('#table');

        $('#gnrt').click(function(){
            x.empty();
            var qty = $('#qty').val();
            for(let i=0; i<qty; i++){
                x.append(`
                    <tr>
                        <td>`+(i+1)+`</td>
                        <td><input type="text" name="fam_code[`+i+`]" class="form-control"></td>
                    </tr>
                `);
            }
            // $.each(function(i){
            //     x.append(`
            //         <tr>
            //             <td>`+(i+1)+`</td>
            //             <td><input type="text" name="fam_code[`+(i+1)+`]" class="form-control"></td>
            //         </tr>
            //     `);
            // });


        });
        //====================================================================================


    });
</script>

@endsection
