@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Vendor</h3>
        </div>
    </div>


    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalvendor">
                ADD
            </button>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-striped text-center">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>name</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($gets as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>
                                <a href="{{url('itasset/master/vendor/edit/'.$item->id)}}" class="btn btn-warning">Edit</a>
                                <a href="{{url('itasset/master/vendor/delete/'.$item->id)}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modalvendor" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item Type</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Vendor</label>
                    <input type="text" name="" class="form-control" id="vendorName">
                </div>
                <div class="form-group d-grid mt-2">
                    <button type="button" id="btnaddvendor" class="btn btn-success">
                        Add
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){

    var vendorName = $('#vendorName');

    $('#btnaddvendor').click(function(){
        $.post("{{url('/itasset/jq/addvendor')}}",
            {
                name: vendorName.val()
            },
            function(data, status){
                location.reload();
                alert("Status: " + status);
            }
        );
    });

});
</script>

@endsection
