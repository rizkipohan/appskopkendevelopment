@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <h3>Item Name</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-6 text-center">
            <form action="{{url('itasset/master/itemname/update/'.$items->id)}}" method="POST">
                @csrf
                <div class="form-group mb-3">
                    <input type="text" name="name" class="form-control" id="" placeholder="Name" value="{{$items->name}}">
                </div>
                <div class="form-group mb-3">
                    <select name="itemtype" class="form-control" id="">
                        <option value="">-Select Item Type-</option>
                        @foreach ($type as $types)
                        <option value="{{$types->id}}"
                            @if ($types->id == $items->item_type_id)
                            selected
                            @endif
                            >{{$types->name}}</option>
                        @endforeach
                    </select>
                </div>
                {{-- <div class="form-group">
                    <input type="text" name="code_for_asset" class="form-control" id="" placeholder="Code for IT Asset" value="{{$items->code_for_asset}}">
                </div> --}}
                <div class="form-group d-grid mt-2">
                    <button type="submit" id="btnadditemtype" class="btn btn-success">
                        Update
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
