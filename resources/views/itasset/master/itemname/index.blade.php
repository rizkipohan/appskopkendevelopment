@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">ITEM NAME</h3>
        </div>
    </div>


    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalitemname">
                ADD
            </button>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-striped text-center">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>name</th>
                        <th>type</th>
                        {{-- <th>code for asset</th> --}}
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($gets as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->itemtype['name']}}</td>
                            {{-- <td>{{$item->code_for_asset}}</td> --}}
                            <td>
                                <a href="{{url('itasset/master/itemname/edit/'.$item->id)}}" class="btn btn-warning">Edit</a>
                                <a href="{{url('itasset/master/itemname/delete/'.$item->id)}}" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="modalitemname" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Item Type</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Select Item Type</label>
                    <select name="" class="custom-select" id="itemType">
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Item Name</label>
                    <input type="text" name="" class="form-control" id="itemName">
                </div>
                {{-- <div class="form-group">
                    <label for="">Code For Asset</label>
                    <input type="text" name="" class="form-control" id="codeasset">
                </div> --}}
                <div class="form-group d-grid mt-2">
                    <button type="button" id="btnadditemtype" class="btn btn-success">
                        Add
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$(document).ready(function(){

    var itemName = $('#itemName');
    var itemType = $('#itemType');
    var code_asset = $('#codeasset');

    $.get("{{url('/itasset/jq/getitemtype')}}",
        function(data){
            itemType.append(data);
        }
    );
    itemType.select2({ selectOnClose: true, theme: "bootstrap"});


    $('#btnadditemtype').click(function(){
        $.post("{{url('/itasset/jq/additemname')}}",
            {
                name: itemName.val(),
                itemtype: itemType.val(),
                code_for_asset: code_asset.val(),
            },
            function(data, status){
                location.reload();
                alert("Status: " + status);
            }
        );
    });

});
</script>

@endsection
