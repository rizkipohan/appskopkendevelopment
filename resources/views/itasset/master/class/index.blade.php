@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <h3>Class</h3>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#modalclass">
                ADD
            </button>
        </div>
    </div>

    <div class="modal fade" id="modalclass" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Class</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-group mb-3">
                        <input type="text" name="" class="form-control" id="classname" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="text" name="" class="form-control" id="codeforasset" placeholder="Code for IT Asset">
                    </div>
                    <div class="form-group d-grid mt-2">
                        <button type="button" id="btnadditemtype" class="btn btn-success">
                            Add
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table text-center">
                <thead>
                    <tr>
                        <th>no</th>
                        <th>name</th>
                        <th>code in asset</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($gets as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->code_for_asset}}</td>
                            <td>
                                <a href="{{url('itasset/master/class/delete/'.$item->id)}}" class="btn btn-danger">Delete</a>
                                <a href="{{url('itasset/master/class/edit/'.$item->id)}}" class="btn btn-warning">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

    var x = $('#itemtype');

    $('#btnadditemtype').click(function(){
        var vx = $('#classname');
        var vy = $('#codeforasset');
        $.post("{{url('/itasset/jq/addclass')}}",
        {
            name: vx.val(),
            code_for_asset: vy.val(),
        },
        function(data, status){
            location.reload();
            vx.empty();
            alert("Status: " + status);
        });
    });

});
</script>

@endsection
