@extends('itasset.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-4">
            <h3 class="text-center">Item Detail</h3>
        </div>
    </div>


    {{-- <div class="row justify-content-center">
        <div class="col-md-4 text-center">
            <button type="button" class="btn btn-success" data-bs-toggle="" data-bs-target="">
                Export
            </button>
        </div>
    </div> --}}

    <div class="row justify-content-center">
        <div class="col-md-10">
            <table class="table table-striped" id="data-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Item Name</th>
                        <th>Item Type</th>
                        <th>Serial Number</th>
                        <th>IT Asset Code</th>
                        <th>FAM Code</th>
                        {{-- <th>Action</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($gets as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->itemname->name}}</td>
                            <td>{{$item->itemtype->name}}</td>
                            <td>{{$item->serial_number}}</td>
                            <td>{{$item->it_asset}}</td>
                            <td>{{$item->fam_code}}</td>
                            {{-- <td></td> --}}
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        // var oTable = $('#data-table').DataTable({
        //     processing: true,
        //     serverSide: true,
        //     ajax: {
        //         url: '{{ url("itasset/itemin/dataitemin") }}'
        //     },
        //     columns: [
        //     {data: 'DT_RowIndex', orderable: false, searchable: false},
        //     {data: 'pr_number', name: 'pr_number'},
        //     {data: 'itemname', name: 'itemname'},
        //     {data: 'itemtype', name: 'itemtype'},
        //     ]
        // });
    });
</script>

@endsection
