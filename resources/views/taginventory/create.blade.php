@extends('taginventory.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="{{url('taginventory/store')}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="">Item Name</label>
                    <select name="item_id" id="" class="custom-select">
                        <option value=""></option>
                        @foreach ($items as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                        @endforeach
                    </select>
               </div>

               <div class="form-group">
                    <label for="">Serial Number</label>
                    <input type="text" name="serial_number" class="form-control"/>
               </div>

               <div class="form-group">
                    <label for="">Received from vendor at:</label>
                    <input type="text" class="form-control" id="received_at" name="received_at" value="{{date('Y-m-d')}}">
                    <script type="text/javascript">
                        $("#received_at").datepicker({
                            format: 'yyyy-mm-dd',
                            orientation: 'bottom',
                            autoclose: true
                        });
                    </script>
               </div>

               <div class="form-group">
                   <button type="submit" class="btn btn-success btn-block">add</button>
               </div>

           </form>
        </div>
    </div>
</div>
@endsection
