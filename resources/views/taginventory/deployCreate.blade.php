@extends('taginventory.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            <form action="{{url('taginventory/deploy/update/'.$tags->id)}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="">Item Name</label>
                    <input type="text" class="form-control" name="" id="" value="{{$tags->item['name']}}" readonly>
                    <input type="hidden" name="item_id" value="{{$tags->item_id}}">
                </div>
                <div class="form-group">
                    <label for="">Serial Number</label>
                    <input type="text" class="form-control" name="" id="" value="{{$tags->serial_number}}" readonly>
                </div>
                <div class="form-group">
                    <label for="">Deployed To</label>
                    <select name="store_id" class="custom-select" id="store">
                        <option value="" selected></option>
                    @foreach ($stores as $store)
                        <option value="{{$store->id}}"
                        @if($tags->store_id == $store->id)    
                          selected  
                        @endif>{{$store->store_name}}</option>
                    @endforeach
                    </select>
                    <script>
                        $("#store").select2({
                            selectOnClose: true,
                            theme: "bootstrap"
                        });
                    </script>
                </div>
                <div class="form-group">
                    <label for="">Deploy Date</label>
                    <input type="text" class="form-control" id="date" name="deployed_at"
                    @isset($tags->deployed_at)
                        value="{{$tags->deployed_at}}"    
                    @endisset>
                    <script type="text/javascript">
                        $("#date").datepicker({
                            format: 'yyyy-mm-dd',
                            orientation: 'bottom',
                            autoclose: true
                        });
                    </script>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Update</button>
                </div>
            </form>
            
        </div>
    </div>
</div>
@endsection