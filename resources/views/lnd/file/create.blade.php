@extends('lnd.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="">
                <div class=""></div>

                <div class="">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <h4 class="text-center">Upload a New File</h4>

                    <form action="{{ url('/file/upload') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="">Select Folder</label>
                            <select name="folder" id="" class="custom-select" required>
                                <option value=""></option>
                                @foreach ($folders as $folder)
                                <option value="{{$folder->id}}">{{$folder->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="file" name="file">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                                <script>
                                    $('#file').on('change',function(){
                                        //get the file name
                                        var fileName = $(this).val();
                                        //replace the "Choose a file" label
                                        $(this).next('.custom-file-label').html(fileName);
                                        // $('#rename').val(fileName);
                                    })
                                </script>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="text" name="rename" id="rename" class="form-control" placeholder="Rename the uploaded file" required>
                        </div>

                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-success">Upload File</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection