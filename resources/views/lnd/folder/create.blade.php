@extends('lnd.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="">
                <div class=""></div>

                <div class="">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <h4 class="text-center">Create a New Folder</h4>

                    <form action="{{ url('/folder/store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="">Folder Name:</label>
                            <input type="text" name="name" class="form-control" id="" required>
                        </div>
                        
                        <div class="row justify-content-center">
                            <button type="submit" class="btn btn-success">Create Folder</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection