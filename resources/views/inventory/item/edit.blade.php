@extends('inventory.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Edit item</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{ url('inventory/item/update/'.$item->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Item Name</label>
                            <input type="text" class="form-control" name="name" id="" value="{{$item->name}}" required>
                        </div>
                        <div class="form-group">
                            <label for="">Price</label>
                            <input type="text" class="form-control" name="price" id="" value="{{$item->price}}">
                        </div>
                        {{-- <div class="form-group">
                            <label for="">Show</label>
                            <select name="show" id="" class="custom-select">
                                <option value="0">no</option>
                                <option value="1">yes</option>
                            </select>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label for="">Qty</label>
                            <input type="number" class="form-control" name="qty" id="" min="0" value="{{$item->qty}}" required>
                        </div> --}}
                        <button type="submit" class="btn btn-success">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection