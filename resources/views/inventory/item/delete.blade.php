@extends('inventory.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center"><h3>are you sure to delete this data?</h3></div>

                <div class="card-body">
                <form action="{{ url('inventory/item/destroy/'.$item->id) }}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-danger btn-block">Yes</button>
                    <a class="btn btn-success btn-block" href="{{ url('inventory/item/index') }}">No</a>
                </div>
            </div>
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" name="store_name" id="" value="{{$item->name}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="">Qty</label>
                        <input type="text" class="form-control" name="store_code" id="" value="{{$item->qty}}" readonly>
                    </div>
                
                </form>
        </div>
    </div>
</div>
@endsection