@extends('inventory.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create New Item</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                   <form action="{{url('/inventory/item/store')}}" method="POST" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="">Item Name</label>
                            <input type="text" class="form-control" name="name" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Price</label>
                            <input type="number" class="form-control" name="price" id="" min="0">
                        </div>
                        {{-- <div class="form-group">
                            <label for="">Qty</label>
                            <input type="number" class="form-control" name="qty" id="" min="0" required>
                        </div> --}}
                        <button type="submit" class="btn btn-success">Create</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection