@extends('inventory.app')

@section('content')
<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Item In History</h1>
        </div>
    </div>
 
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="">
                {{-- <div class="">Store List</div> --}}
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal">
                    <i class="fa fa-search" aria-hidden="true"></i> Search 
                </button>

                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Search</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{url('inventory/itemin/search')}}" method="get" autocomplete="off">
                                    @csrf
                                    <div class="form-group row mb-3">
                                        <label for="startDate" class="col-md-4 col-form-label text-md-right">
                                            Search
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="key" id="key" aria-describedby="helpId" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group row justify-content-center">
                                        <button type="submit" class="col-md-6 btn btn-success btn-block">Search</button>
                                    </div>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="">
                    {{-- <div class="form-inline my-3 justify-content-center">
                        {{$items->links()}}
                    </div> --}}
                    <div class="d-flex justify-content-center">
                        <a name="" id="" class="btn btn-success my-2" href="{{url('inventory/itemin/create')}}" role="button">Add Item In</a>
                    </div>
                    <table class="table table-hover" class="table-striped" id="table">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Item Name</th>
                                    <th>Qty</th>
                                    <th>From</th>
                                    <th>Vendor</th>
                                    <th>Store</th>
                                    <th>PO Number</th>
                                    <th>PR Number</th>
                                    <th>Date/Time</th>
                                    <th>Serial. No</th>
                                    <th>Notes</th>
                                    <th>Action</th>
                                </tr> 
                            </thead>
                            <tbody>
                                @if(count($items)==0)
                                <tr class="text-center"><td colspan="100">No Data</td></tr>
                                @else
                                    @foreach ($items as $key => $item)
                                    <tr class="text-center">
                                        <td>{{$key+1}}</td>
                                        <td>{{$item->item['name']}}</td>
                                        <td>{{$item->qty}}</td>
                                        <td>{{$item->from}}</td>
                                        <td>{{$item->vendor}}</td>
                                        <td>@isset($item->store_id) {{$item->store->store_name}} @endisset</td>
                                        <td>{{$item->po_number}}</td>
                                        <td>{{$item->pr_number}}</td>
                                        <td>{{$item->date}}</td>
                                        <td>{{$item->serial_number}}</td>
                                        <td>{{$item->notes}}</td>
                                        <td>    
                                            <a class="btn btn-warning" href="{{url('inventory/itemin/edit/'.$item->id)}}">Edit</a>                                        
                                            <a class="btn btn-danger" href="{{url('inventory/itemin/delete/'.$item->id)}}">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // const queryString = window.location.search;
    // const urlParams = new URLSearchParams(queryString);
    // const page = urlParams.get('page');
    // const key = urlParams.get('key');
    // document.cookie = "pageItemIn=" + page;

    // $('#key').val(key);

    $(document).ready(function(){
        $('#table').DataTable();
    });
</script>
@endsection
