<h2>Item name: {{$itemout[0]->item->name}}</h2>
<br>
<br>
<h4>Detail Transaction In</h4>
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Item Name</th>
            <th>Qty</th>
            <th>From</th>
            <th>Vendor</th>
            <th>Store</th>
            <th>PO Number</th>
            <th>PR Number</th>
            <th>Date/Time</th>
            <th>Serial. No</th>
            <th>Notes</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($itemin as $item)    
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->item->name}}</td>
            <td>{{$item->qty}}</td>
            <td>{{$item->from}}</td>
            <td>{{$item->vendor}}</td>
            <td>@isset($item->store_id) {{$item->store->store_name}} @endisset</td>
            <td>{{$item->po_number}}</td>
            <td>{{$item->pr_number}}</td>
            <td>{{$item->date}}</td>
            <td>{{$item->serial_number}}</td>
            <td>{{$item->notes}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
<br>
<br>
<h4>Detail Transaction Out</h4>
<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Item Name</th>
            <th>Qty</th>
            <th>To</th>
            <th>Status</th>
            <th>Date</th>
            <th>Serial. No</th>
            <th>Notes</th>
            <th>Reason</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($itemout as $item)    
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->item->name}}</td>
            <td>{{$item->qty}}</td>
            <td>{{$item->store['store_name']}}</td>
            <td>{{$item->status}}</td>
            <td>{{$item->date}}</td>
            <td>{{$item->serial_number}}</td>
            <td>{{$item->notes}}</td>
            <td>{{$item->reason}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
<br>
<br>
<br>
<h4>Total in and out transaction</h4>
<table>
    <thead>
        <tr>
            <th>Total In</th>
            <th>Total Out</th>
            <th>Balance</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{$itemin->sum('qty')}}</td>
            <td>{{$itemout->sum('qty')}}</td>
            <td>{{$itemin->sum('qty')-$itemout->sum('qty')}}</td>
        </tr>
    </tbody>
</table>