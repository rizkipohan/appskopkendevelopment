@extends('inventory.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Report Item Quantity</h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if (count($items)>0)
            <div class="d-flex justify-content-center">
                <a name="" id="" class="btn btn-success my-2" href="{{url('inventory/report/itembalance/export/'.$dpt)}}" role="button">Export All To Excel</a>
            </div>
            @endif
           
            <table class="table table-hover text-center" class="table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Item Name</th>
                        {{-- <th>Price</th> --}}
                        <th>In</th>
                        <th>Out</th>
                        <th>Balance</th>
                        {{-- <th>Action</th> --}}
                        <th>Detail</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($items)==0)
                    <tr><td colspan="100">No Data</td></tr>
                    @else
                        @foreach ($items as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            {{-- <td>{{$item->price}}</td> --}}
                            <td>{{$item->itemin->sum('qty')}}</td>
                            <td>{{$item->itemout->sum('qty')}}</td>
                            <td>{{$item->itemin->sum('qty') - $item->itemout->sum('qty')}}</td>
                            {{-- <td>
                                <a class="btn btn-warning" href="{{url('inventory/item/edit/'.$item->id)}}">Edit</a> | 
                                <a class="btn btn-danger" href="{{url('inventory/item/delete/'.$item->id)}}">Delete</a>
                            </td> --}}
                            <td><a href="{{url('/inventory/report/itemdetail/export/'.$item->id)}}" class="btn btn-info">Export In/Out Detail</a></td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
        </table>
        </div>
    </div>
</div>
@endsection