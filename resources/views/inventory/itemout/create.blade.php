@extends('inventory.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
            
            <br>
            
            <h1 class="text-center">Item Out Transaction</h1>
        </div>
    </div>
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                   <form action="{{url('/inventory/itemout/store')}}" method="POST" autocomplete="off">
                        @csrf
                        <div class="form-group">
                            <label for="">Item Name</label>
                            <select name="item" class="custom-select" id="" required>
                                <option value="" selected></option>
                            @foreach ($items as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Qty</label>
                            <input type="number" class="form-control" name="qty" id="" min="0" value="1" required>
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            <select name="status" class="custom-select" id="from" required>
                                <option value="">-</option>
                                <option value="nso" selected>N.S.O</option>
                                <option value="replace">replace</option>
                                <option value="service">service</option>
                                <option value="employee">employee</option>
                                <option value="others">others</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">To Store</label>
                            <select name="store" class="custom-select" id="">
                                <option value="" selected></option>
                            @foreach ($stores as $store)
                                <option value="{{$store->id}}">{{$store->store_name}}</option>
                            @endforeach
                            </select>
                            <script>
                                $("*[name='store']").select2({
                                    selectOnClose: true,
                                    theme: "bootstrap"
                                });
                            </script>
                        </div>
                        <div class="form-group">
                            <label for="">Employee Name</label>
                            <input type="text" name="employee_name" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Serial Number</label>
                            <input type="text" name="serial_number" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Notes</label>
                            <input type="text" name="notes" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Reason</label>
                            <input type="text" name="reason" class="form-control" id="">
                        </div>
                        <div class="form-group">
                            <label for="">Date</label>
                            <input type="text" class="form-control" id="date" name="date">
                            <script type="text/javascript">
                                $("#date").datepicker({
                                    format: 'yyyy-mm-dd',
                                    orientation: 'bottom',
                                    autoclose: true
                                });
                            </script>
                        </div>
                        <button type="submit" class="btn btn-success">Create</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection