@extends('storeproperties.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10 mb-2">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif

            @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong class="text-center">{{ $message }}</strong>
            </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col">
            @if ($errors->any())
                <div class="alert alert-warning alert-dismissible fade show">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <form action="{{ url('storeprops/store') }}" method="POST" class="" autocomplete="off">
                @csrf

                <h4>STORE</h4>
                <hr>
                <div class="row">
                    <div class="form-group col-4">
                        <label for="">Store Name</label>
                        <select name="store_name" class="custom-select" id="store_name" required>
                            <option value="" selected>-</option>
                            @foreach ($stores as $store)
                                <option value="{{$store->store_name}}"
                                    data-code="{{$store->store_code}}"
                                    data-id="{{$store->id}}">
                                    {{$store->store_name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Store Code</label>
                        <input type="text" name="store_code" class="form-control" id="store_code">
                        <input type="hidden" name="store_id" class="form-control" id="store_id">
                    </div>
                    <div class="form-group col-4">
                        <label for="">Email Store</label>
                        <input type="text" name="email" class="form-control" id="email_store">
                    </div>
                    <div class="form-group col-4">
                        <label for="">Open Date</label>
                        <input type="text" name="open_date" class="form-control" id="open_date">
                        <script type="text/javascript">
                            $("#open_date").datepicker({
                                format: 'yyyy-mm-dd',
                                orientation: 'bottom',
                                autoclose: true
                            });
                        </script>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Store PIC Name</label>
                        <input type="text" name="pic_name" class="form-control" id="" required>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Store PIC Email</label>
                        <input type="email" name="pic_email" class="form-control" id="" required>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Store PIC Phone</label>
                        <input type="text" name="pic_phone" class="form-control" id="">
                    </div>
                    <script>
                        $("#store_name").select2({
                            selectOnClose: true,
                            theme: "bootstrap"
                        });
                        $("#store_name").change(function(){
                            // console.log($("#store_name").find(':selected').data('code'))
                            $("#store_code").val($(this).find(':selected').data('code'));
                            $("#store_id").val($(this).find(':selected').data('id'));
                            $("#apps_id").val($(this).find(':selected').data('code'));
                            $("#email_store").val($(this).find(':selected').data('code').toLowerCase()+"@kopikenangan.com");
                        });
                    </script>
                </div>

                <div class="row">

                </div>

                <h4>INTERNET</h4>
                <hr>
                <div class="row">
                    <div class="col form-group">
                        <label for="">Status</label>
                        <select name="internet" class="custom-select" id="">
                            <option value="">-</option>
                            <option value="on progress">on progress</option>
                            <option value="on activation">on activation</option>
                            <option value="done">done</option>
                        </select>
                    </div>
                    <div class="col form-group">
                        <label for="">Provider</label>
                        <input type="text" name="isp" class="form-control">
                    </div>
                    <div class="col">
                        <label for="">SID</label>
                        <input type="text" name="sid" class="form-control">
                    </div>
                    <div class="col form-group">
                        <label for="">IP Public</label>
                        <input type="text" name="ip_public" class="form-control">
                    </div>
                    <div class="col form-group">
                        <label for="">IP Reserved</label>
                        <input type="text" name="ip_reserved" class="form-control">
                    </div>
                </div>
                <div class="row">

                </div>

                <h4>Outlet Apps</h4>
                <hr>
                <div class="row">
                    <div class="form-group col">
                        <label for="">ID</label>
                        <input type="text" name="apps_id" class="form-control" id="apps_id">
                    </div>
                    <div class="form-group col">
                        <label for="">PIN</label>
                        <input type="text" name="apps_pin" class="form-control">
                    </div>
                </div>

                <h4>CCTV</h4>
                <hr>
                <div class="row">
                    <div class="col form-group">
                        <label for="">Recording Status</label>
                        <select class="custom-select" name="cctv_recording" id="">
                            <option selected>-</option>
                            <option value="recorded">recorded</option>
                        </select>
                    </div>
                    <div class="col form-group">
                        <label for="">Camera Qty</label>
                        <input type="number" name="cctv_cam_qty" class="form-control" min="0">
                    </div>
                    <div class="col form-group">
                        <label for="">Cloud ID</label>
                        <input type="text" name="cctv_cloud_id" class="form-control" id="">
                    </div>
                    <div class="col form-group">
                        <label for="">Streaming Status</label>
                        <select name="cctv_streaming" class="custom-select" id="">
                            <option value="">-</option>
                            <option value="on progress">on progress</option>
                            <option value="on activation">on activation</option>
                            <option value="active">active</option>
                        </select>
                    </div>
                    <div class="col form-group">
                      <label for="">Cloud Regional</label>
                      <select name="cctv_regional" id="" class="custom-select">
                          <option value="">-</option>
                          <option value="regional 1">regional 1</option>
                          <option value="regional 2">regional 2</option>
                          <option value="regional 3">regional 3</option>
                      </select>
                    </div>
                </div>

                <h4>PERIPHERAL</h4>
                <hr>
                <div class="row">
                    <div class="form-group col-4">
                        <label for="">PoS</label>
                        <select name="pos" id="" class="custom-select">
                            <option value=""></option>
                            <option value="running">running</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">PoS WiFi</label>
                        <select name="pos_wifi" id="" class="custom-select">
                            <option value=""></option>
                            <option value="installed">installed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Wifi Extended Cable</label>
                        <select name="wifi_extended_cable" id="" class="custom-select">
                            <option value=""></option>
                            <option value="installed">installed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Printer Label</label>
                        <select name="printer_label" id="" class="custom-select">
                            <option value=""></option>
                            <option value="running">running</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Tablet</label>
                        <select name="tablet" id="" class="custom-select">
                            <option value=""></option>
                            <option value="running">running</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">Laptop</label>
                        <select name="laptop" id="" class="custom-select">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">T-hub type</label>
                        <select name="t_hub" id="" class="custom-select">
                            <option value=""></option>
                            <option value="type c">type c</option>
                            <option value="micro usb">micro usb</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">PoS Trainer</label>
                        <input type="text" name="pos_trainer" class="form-control" id="">
                    </div>
                    <div class="form-group col-4">
                        <label for="">USB LCD Content</label>
                        <select name="usb_lcd_content" id="" class="custom-select">
                            <option value=""></option>
                            <option value="running">running</option>
                        </select>
                    </div>
                </div>

                <h4>OPERATIONAL</h4>
                <hr>
                <div class="row">
                    <div class="form-group col-4">
                        <label for="">code creating order</label>
                        <select name="code_creating_order" id="" class="form-control">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">create offline order</label>
                        <select name="create_offline_order" id="" class="form-control">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">check order appear in apps</label>
                        <select name="check_order_appear_in_apps" id="" class="form-control">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">scan qr code create order</label>
                        <select name="scan_qr_code_create_order" id="" class="form-control">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">data product and promo</label>
                        <select name="data_product_promo" id="" class="form-control">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">employee register</label>
                        <select name="employee_register" id="" class="form-control">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                    <div class="form-group col-4">
                        <label for="">employee tarining</label>
                        <select name="employee_training" id="" class="form-control">
                            <option value=""></option>
                            <option value="completed">completed</option>
                        </select>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary btn-block">Create</button>

            </form>


        </div>
    </div>
</div>
@endsection
