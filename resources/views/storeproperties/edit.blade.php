@extends('master.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit store properties</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-warning alert-dismissible fade show">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    
                    <form action="{{ url('master/store/update/'.$store->id) }}" method="POST" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="form-group col-4">
                                <label for="">Store Name</label>
                                <input type="text" class="form-control" name="store_name" id="" value="{{$store->store_name}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">Store Code</label>
                                <input type="text" class="form-control" name="store_code" id="" value="{{$store->store_code}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">Store Status</label>
                                <select class="custom-select" name="store_status" id="">
                                    <option value="Close">Close</option>
                                    <option value="Open" selected>Open</option> 
                                    {{-- <option value="2">Disabled</option> --}}
                                </select>
                            </div>

                            <div class="form-group col-4">
                                <label for="" class="">Open Date</label>
                                <div class="">
                                    <input type="text" class="form-control" id="open_date" value="{{$store->open_date}}" name="open_date">
                                    <script type="text/javascript">
                                        $("#open_date").datepicker({
                                            format: 'yyyy-mm-dd',
                                            orientation: 'bottom',
                                            autoclose: true
                                        });
                                    </script>
                                </div>
                            </div>

                            <div class="form-group col-4">
                            <label for="">Store Address</label>
                            <textarea class="form-control" name="store_address" id="" rows="3">{{$store->store_address}}</textarea>
                            </div>

                            <div class="form-group col-4">
                            <label for="">Store Email</label>
                            <input type="email" class="form-control" name="store_email" id="" value="{{$store->store_email}}">
                            </div>

                            <div class="form-group col-4">
                            <label for="">Store Phone</label>
                            <input type="text" name="store_phone" id="" class="form-control" value="{{$store->store_phone}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">Tax Number (NOPD)</label>
                                <input type="text" name="tax_number" id="" class="form-control" value="{{$store->tax_number}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">Connection Status</label>
                                <select name="connection_status" class="custom-select" id="">
                                    <option value=""
                                    @if($store->connection_status == null)
                                        selected
                                    @endif></option>
                                    <option value="On Provisioning"
                                    @if($store->connection_status == "On Provisioning")
                                        selected
                                    @endif>On Provisioning</option>
                                    <option value="On Activation"
                                    @if($store->connection_status == "On Activation")
                                        selected
                                    @endif>On Activation</option>
                                    <option value="Online"
                                    @if($store->connection_status == "Online")
                                        selected
                                    @endif>Online</option>
                                </select>

                            </div>

                            <div class="form-group col-4">
                                <label for="">Internet Provider</label>
                                <input type="text" class="form-control" name="provider" id="" value="{{$store->provider}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">SID</label>
                                <input type="text" name="sid" id="" class="form-control" value="{{$store->sid}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">Mikrotik Status</label>
                                <select name="mikrotik_status" class="custom-select" id="">
                                    <option value="" 
                                    @if($store->mikrotik_status == null)
                                        selected
                                    @endif
                                    ></option>
                                    <option value="Sudah Dikirim"
                                    @if($store->mikrotik_status == "Sudah Dikirim")
                                        selected
                                    @endif>Sudah Dikirim</option>
                                </select>
                            </div>

                            <div class="form-group col-4">
                                <label for="">IP Local</label>
                                <input type="text" name="ip_local" id="" class="form-control" value="{{$store->ip_local}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">IP WAN</label>
                                <input type="text" name="ip_wan" id="" class="form-control" value="{{$store->ip_wan}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">IP Gateway WAN</label>
                                <input type="text" name="ip_gateway_wan" id="" class="form-control" value="{{$store->ip_gateway_wan}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">IP DNS</label>
                                <input type="text" name="ip_dns" id="" class="form-control" value="{{$store->ip_dns}}">
                            </div>

                            <div class="form-group col-4">
                                <label for="">IP VPN</label>
                                <input type="text" name="ip_vpn" id="" class="form-control" value="{{$store->ip_vpn}}">
                            </div>
                        </div>
                        
                        <button type="submit" class="btn btn-success">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection