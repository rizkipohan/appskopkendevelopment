<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemOutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('item_out', function (Blueprint $table) {
        //     $table->id();
        //     $table->foreignId('item_id')->nullable();
        //     $table->foreignId('store_id')->nullable();
        //     $table->integer('qty')->nullable();
        //     $table->string('status')->nullable();
        //     $table->string('serial_number')->nullable();
        //     $table->string('notes')->nullable();
        //     $table->date('date')->nullable();
        //     $table->timestamps();

        //     $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');
        //     $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade')->onUpdate('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_out');
    }
}
