<?php

use Illuminate\Database\Seeder;
use App\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create([
            'name' => 'unknown'
        ]);

        Department::create([
            'name' => 'IT'
        ]);

        Department::create([
            'name' => 'OPS Service'
        ]);

        Department::create([
            'name' => 'Facility'
        ]);

    }
}
